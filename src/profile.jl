module Profile

const PROFILE=false

mutable struct Timings
    parent::Union{Timings,Nothing}
    timer::Float64
    accumulated::Float64
    children::Dict{Any,Timings}
end

const root=Timings(nothing,0,0,Dict{Any,Timings}())
const current=Ref(root)

function start(key)
    if PROFILE
        if !haskey(current[].children,key)
            current[].children[key]=Timings(current[],0,0,Dict{Any,Timings}())
        end
        current[]=current[].children[key]
        current[].timer=time()
    end
end

function stop()
    if PROFILE
        current[].accumulated=current[].accumulated+time()-current[].timer
        if current[].parent!=nothing
            current[]=current[].parent
        end
    end
end

function clear()
    if PROFILE
        current[]=root
        root.accumulated=0
        empty!(root.children)
    end
end

function dump()
    F=open("profile.org","w")

    function dump1(level,key,t)
        write(F,"*"^level*" $(key) $(t.accumulated)\n")
        for k in sort(collect(keys(t.children)),by=x->-t.children[x].accumulated)
            dump1(level+1,k,t.children[k])
        end
    end

    dump1(1,"root",root)
    
    close(F)
end

end
