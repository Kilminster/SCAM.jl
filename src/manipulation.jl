module Manipulation

import ..@symbolic
import ..@symbolic0
import ..Symbolics: Curly,Application,Atom,Concrete
import ..Rewriters
import ..Rewriters: RewriterList,Rule,Reduce,Group,SubContext,FunctionWrap
import ..SymbolicFunctions: isConcreteInteger,commutesMultiplicatively
import ..defaultregistry
import ..Registries: register

const checktrue=Rewriters.CheckTrue(Reduce(Group(:standard)))
const standard=Reduce(Group(:standard))

register(defaultregistry,:manipulation,
         RewriterList([# subst
                       
                       Rule((@symbolic _strict{subst{X,Y}(X)}),
                            (@symbolic Y),
                            :X,:Y),
                       
                       Rule((@symbolic _strict{subst{X,Y}(F(Z...))}),
                            (@symbolic subst{X,Y}{subst{X,Y}(F)()}{Z...}),
                            :X,:Y,:F,:Z),
                       
                       Rule((@symbolic subst{X,Y}{F(Z...)}{W1,W...}),
                            (@symbolic subst{X,Y}{F(Z...,subst{X,Y}(W1))}{W...}),
                            :X,:Y,:F,:Z,:W1,:W),
                       
                       Rule((@symbolic subst{X,Y}{F}{}),
                            (@symbolic F),
                            :X,:Y,:F),
                       
                       Rule((@symbolic _strict{subst{X,Y}(Z)}),
                            (@symbolic Z),
                            :X,:Y,
                            :Z=>(z,context)->!((z isa Curly)||(z isa Application))),

                       # active lambda
                       Rule((@symbolic _active{}{}),
                            (@symbolic λ{true,_t})),
                       Rule((@symbolic _active{}{X...}),
                            (@symbolic _active{λ{∨(),_t}}{X...}),:X),
                       Rule((@symbolic _active{λ{∨(T...),_t}}{X1,X2...}),
                            (@symbolic _active{λ{∨(T...,¬isFree{_t,X1}),_t}}{X2...}),:T,:X1,:X2),
                       Rule((@symbolic _active{L}{}),
                            (@symbolic L),:L),

                       # expand
                       Rule((@symbolic expand(F)),
                            (@symbolic expand{}{}(F)),:F),
                       Rule((@symbolic expand{X...}(F)),
                            (@symbolic expand{}{X...}(F)),:X,:F),
                       Rule((@symbolic expand{A1...}{A2...}(F)),
                            (@symbolic F3),
                            :A1,:A2,:F,
                            transform=[:L=>(standard,@symbolic _active{A1...}{A2...}),
                                       :F1=>(SubContext((@symbolic context{F,X...}),
                                                        (@symbolic F),
                                                        standard,
                                                        :F,:X,
                                                        newvalues=(@symbolic _newvalues{X...}),
                                                        groupadditions=Dict(:standard=>Set([:expand1]))),
                                             @symbolic context{F,active,L}),
                                       :F2=>(SubContext((@symbolic context{F,X...}),
                                                        (@symbolic F),
                                                        standard,
                                                        :F,:X,
                                                        newvalues=(@symbolic _newvalues{X...}),
                                                        groupadditions=Dict(:standard=>Set([:expand2]))),
                                             @symbolic context{F1,active,L}),
                                       :F3=>(SubContext((@symbolic context{F,X...}),
                                                        (@symbolic F),
                                                        standard,
                                                        :F,:X,
                                                        newvalues=(@symbolic _newvalues{X...}),
                                                        groupadditions=Dict(:standard=>Set([:expand3]))),
                                             @symbolic context{F2,active,L}),
                                       ]),
                                       
                       # mapcoeff:
                       Rule((@symbolic mapcoeff{X...}(F)),
                            (@symbolic mapcoeff{}{X...}(F)),:X,:F),
                       Rule((@symbolic mapcoeff{A1...}{A2...}(F)),
                            (@symbolic λ{R2,_C}),
                            :A1,:A2,:F,
                            transform=[:L=>(standard,@symbolic _active{A1...}{A2...}),
                                       :R1=>(SubContext((@symbolic context{F,X...}),
                                                        (@symbolic F),
                                                        standard,
                                                        :F,:X,
                                                        newvalues=(@symbolic _newvalues{X...}),
                                                        groupadditions=Dict(:standard=>Set([:mapcoeff1]))),
                                             @symbolic context{F,active,L}),
                                       :R2=>(SubContext((@symbolic context{F,X...}),
                                                        (@symbolic F),
                                                        standard,
                                                        :F,:X,
                                                        newvalues=(@symbolic _newvalues{X...}),
                                                        groupadditions=Dict(:standard=>Set([:mapcoeff2]))),
                                             @symbolic context{R1,active,L}),
                                       ]
                            ),

                       # polynomial freeze/thaw:
                       Rule((@symbolic _strict{polyFreeze(+(X,Y...))}),
                            (@symbolic polyFreeze(X)+polyFreeze(+(Y...))),:X,:Y),

                       Rule((@symbolic _strict{polyFreeze(*(X,Y...))}),
                            (@symbolic polyFreeze(X)*polyFreeze(*(Y...))),:X,:Y),

                       Rule((@symbolic _strict{polyFreeze(X^+(A,B...))}),
                            (@symbolic polyFreeze(X^A)*polyFreeze(X^*(B...))),:X,:A,:B),

                       Rule((@symbolic _strict{polyFreeze(X^singletonwrap(*,*(commute(N),R...)))}),
                            (@symbolic polyFreeze(X^(*(N,R...)/P))^P),
                            :X,:R,:N=>(s,c)->s isa Concrete{Rational{BigInt}},
                            transform=[:P=>(FunctionWrap((s,c)->Concrete(Rational{BigInt}(abs(s.value.num)))),@symbolic N),
                                       :_=>(checktrue,@symbolic P>1)]),

                       Rule((@symbolic _strict{polyFreeze(X)}),
                            (@symbolic X),:X=>(s,c)->s isa Concrete),

                       Rule((@symbolic _strict{polyFreeze(X)}),
                            (@symbolic 𝗫{X}),:X),

                       Rule((@symbolic _strict{polyThaw(𝗫{X})}),
                            (@symbolic X),:X),

                       Rule((@symbolic _strict{polyThaw(+(X,Y...))}),
                            (@symbolic polyThaw(X)+polyThaw(+(Y...))),:X,:Y),

                       Rule((@symbolic _strict{polyThaw(*(X,Y...))}),
                            (@symbolic polyThaw(X)*polyThaw(*(Y...))),:X,:Y),

                       Rule((@symbolic _strict{polyThaw(X^N)}),
                            (@symbolic polyThaw(X)^N),:X,:N),

                       Rule((@symbolic _strict{polyThaw(X)}),
                            (@symbolic X),:X),
                       
                       ]))



register(defaultregistry,:expand1,
         RewriterList([# Expand out active terms
                       
                       Rule((@symbolic _strict{*(P1...,+(commute(S1),S2,S3...),P2...)}),
                            (@symbolic *(P1...,S1,P2...)+*(P1...,+(S2,S3...),P2...)),
                            :P1,:P2,:S1,:S2,:S3,
                            transform=[:_=>(checktrue,@symbolic contextvalue{active}(S1))]),
                       
                       Rule((@symbolic _strict{(+(commute(S1),S2,S3...))^N}),
                            (@symbolic (S1*(+(S1,S2,S3...))+(+(S2,S3...)*(+(S1,S2,S3...))))^(N/2)),
                            :S1,:S2,:S3,:N=>isConcreteInteger,
                            transform=[:_1=>(checktrue,@symbolic (N>1)∧isInteger(N/2)),
                                       :_2=>(checktrue,@symbolic contextvalue{active}(S1))]),
                       
                       Rule((@symbolic _strict{(+(commute(S1),S2,S3...))^N}),
                            (@symbolic S1*(+(S1,S2,S3...))^(N-1)+(+(S2,S3...))*(+(S1,S2,S3...))^(N-1)),
                            :S1,:S2,:S3,:N=>isConcreteInteger,
                            transform=[:_1=>(checktrue,@symbolic N>1),
                                       :_2=>(checktrue,@symbolic contextvalue{active}(S1))]),
                       
                       Rule((@symbolic _strict{*(commute(+(commute(S1),S2,S3...)^(-1)),commute(P1^N),P2...)}),
                            (@symbolic *((S1*P1^(-N)+(+(S2,S3...))*P1^(-N))^-1,P2...)),
                            :S1=>commutesMultiplicatively,:S2,:S3,
                            :P1=>commutesMultiplicatively,:P2,
                            :N=>isConcreteInteger,
                            transform=[:_1=>(checktrue,@symbolic N<0),
                                       :_2=>(checktrue,@symbolic contextvalue{active}(S1))]),
                       
                       Rule((@symbolic _strict{(+(commute(S1),S2,S3...))^N}),
                            (@symbolic (S1*(+(S1,S2,S3...))^(-N-1)+(+(S2,S3...))*(+(S1,S2,S3...))^(-N-1))^(-1)),
                            :S1=>commutesMultiplicatively,:S2,:S3,:N=>isConcreteInteger,
                            transform=[:_1=>(checktrue,@symbolic N<-1),
                                       :_2=>(checktrue,@symbolic contextvalue{active}(S1))]),
                       
                       ]),incompatible=[:integralsdet,:surdexpressionsdet])
                      

register(defaultregistry,:expand2,
         RewriterList([# Common Denominators
                       Rule((@symbolic +(commute(singletonwrap(*,*(commute(D^N),A...))),commute(singletonwrap(*,*(commute(D^N),B...))),R...)),
                            (@symbolic +((*(A...)+*(B...))*D^N,R...)),
                            :D=>commutesMultiplicatively,:N=>isConcreteInteger,:A,:B,:R,
                            transform=[:_1=>(checktrue,@symbolic N<0)]),
                       ]),incompatible=[:integralsdet,:surdexpressionsdet])

register(defaultregistry,:expand3,
         RewriterList([# Collect like active terms
                       Rule((@symbolic +(commute(A),commute(B),R...)),
                            (@symbolic +(C,R...)),
                            :A,:B,:R,
                            transform=[:_1=>(checktrue,@symbolic contextvalue{active}(A)),
                                       :_2=>(checktrue,@symbolic contextvalue{active}(B)),
#                                       :C=>(SubContext((@symbolic F),
#                                                       (@symbolic F),
#                                                       RewriterList([
#                                                           FunctionWrap((s,c)->println("expand3a: $s")),
#                                                           standard]),
#                                                       :F,
                                       #                                                       groupadditions=Dict(:standard=>Set([:expand3a]))),
                                       :C=>(Reduce(Group(:expand3a)),
                                            @symbolic _collect(A,B)),
                                       :_3=>(checktrue,@symbolic isFree{C,_collect})]),
                       ]),incompatible=[:integralsdet,:surdexpressionsdet])

register(defaultregistry,:expand3a,
         RewriterList([#
                       Rule((@symbolic _collect(singletonwrap(*,*(commute(X),A...)),singletonwrap(*,*(commute(X),B...)),X1...)),
                            (@symbolic _collect(*(A...),*(B...),X1...,X)),
                            :A,:B,:X=>commutesMultiplicatively,:X1,
                            transform=[:_1=>(checktrue,@symbolic contextvalue{active}(X))]),
                       
                       Rule((@symbolic _collect(A,B,X1,X2...)),
                            (@symbolic *(A+B,X1,X2...)),
                            :A,:B,:X1,:X2,
                            transform=[:_1=>(checktrue,@symbolic ¬contextvalue{active}(A)),
                                       :_2=>(checktrue,@symbolic ¬contextvalue{active}(B)),
                                       ]),
                       ]),incompatible=[:integralsdet,:surdexpressionsdet])


register(defaultregistry,:mapcoeff1,
         RewriterList([#
                       Rule((@symbolic *(commute(N),X...)),
                            (@symbolic *(_C(N),X...)),
                            :N=>commutesMultiplicatively,:X,
                            transform=[:_1=>(checktrue,@symbolic isFree{N,_C}),
                                       :_2=>(checktrue,@symbolic ¬contextvalue{active}(N))]),
                       
                       Rule((@symbolic +(commute(N),X...)),
                            (@symbolic +(_C(N),X...)),
                            :N,:X,
                            transform=[:_1=>(checktrue,@symbolic isFree{N,_C}),
                                       :_2=>(checktrue,@symbolic ¬contextvalue{active}(N))]),
                       ]))

register(defaultregistry,:mapcoeff2,
         RewriterList([#
                       Rule((@symbolic *(commute(_C(A)),commute(_C(B)),R...)),
                            (@symbolic *(_C(A*B),R...)),:A,:B,:R),
                       
                       Rule((@symbolic +(commute(_C(A)),commute(_C(B)),R...)),
                            (@symbolic +(_C(A+B),R...)),:A,:B,:R),
                       
                       Rule((@symbolic _C(A)^B),
                            (@symbolic _C(A^B)),:A,:B),
                       ]))
         


end

