module SpecialRewriters

import ..Rewriter
import ..@symbolic
import ..@symbolic0
import ..Callbacks: @stopifdone
import ..Symbolics: Curly,Atom,Application
import ..Symbolic
import ..Patterns
import ..Contexts: newatom

struct LambdaApply <: Rewriter
end

function (r::LambdaApply)(s::Symbolic,context)
    appears(c::Symbolic,x::Symbolic)=false
    appears(c::Atom,x::Symbolic)=(c==x)
    function appears(c::Union{Application,Curly},x::Symbolic)
        if appears(c.head,x)
            return true
        end
        for a in c.args
            if appears(a,x)
                return true
            end
        end
        return false
    end
    
    function unalias(s::Curly,a::Symbolic)
        for v in s.args[2:end]
            if appears(a,v)
                s=replace(s,v.name=>newatom(context,(@symbolic _lambdaapply{s,a,v}),Union{}))
            end
        end
        return s
    end
    
    substitute(s::Symbolic,x::Symbolic,a::Symbolic)=s
    function substitute(s::Atom,x::Symbolic,a::Symbolic)
        if s==x
            return a
        else
            return s
        end
    end
    substitute(s::Application,x::Symbolic,a::Symbolic)=Application(substitute(s.head,x,a),[substitute(arg,x,a) for arg in s.args])
    function substitute(s::Curly,x::Symbolic,a::Symbolic)
        if s.head isa Atom
            if s.head.name==:λ
                s=unalias(s,a)
            end
        end
        Curly(substitute(s.head,x,a),[substitute(arg,x,a) for arg in s.args])
    end

    pat=Patterns.makepattern((@symbolic λ{F,X...}(A...)),:F,:X,:A)
    function results(cb)
        @stopifdone pat(s,Dict{Symbol,Any}(),context)() do bindings
            if length(bindings[:X])==length(bindings[:A])
                if all(x->x isa Atom,bindings[:X])
                    l=Curly((@symbolic λ),[bindings[:F],bindings[:X]...])
                    for i=1:length(bindings[:X])
                        l=unalias(l,bindings[:A][i])
                    end
                    r=l.args[1]
                    X=l.args[2:end]
                    for i=1:length(X)
                        r=substitute(r,X[i],bindings[:A][i])
                    end
                    @stopifdone cb(r)
                end
            end
        end
    end
end

    
end
