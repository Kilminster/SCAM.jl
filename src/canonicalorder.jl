module CanonicalOrder

import ..Symbolics: Symbolic,Concrete,Atom,Application,Curly
import ..Rewriters: Rewriter
import ..Contexts: Context
import ..Callbacks: @stopifdone
import ..SymbolicFunctions: commutesMultiplicatively,isSurdExpression

canonical_order1(a,b)=false
canonical_order1(::Concrete{Rational{BigInt}},::Concrete{Complex{Rational{BigInt}}})=true
canonical_order1(::Concrete{Complex{Rational{BigInt}}},::Concrete{Rational{BigInt}})=false
canonical_order1(a::Atom,b::Atom)=a.name<b.name
canonical_order1(::Concrete,::Atom)=true
canonical_order1(::Atom,::Concrete)=false
canonical_order1(::Concrete,::Application)=true
canonical_order1(::Application,::Concrete)=false
canonical_order1(::Concrete,::Curly)=true
canonical_order1(::Atom,::Curly)=true
canonical_order1(::Curly,::Atom)=false
canonical_order1(::Curly,::Concrete)=false

function canonical_order1(a::Concrete{Rational{BigInt}},b::Concrete{Rational{BigInt}})
    if a.value.den<b.value.den
        return true
    end
    if a.value.den>b.value.den
        return false
    end
    if mod(a.value.num,a.value.den)<mod(b.value.num,b.value.den)
        return true
    end
    if mod(a.value.num,a.value.den)>mod(b.value.num,b.value.den)
        return false
    end
    if a.value.num<b.value.num
        return true
    end
    return false
end

function canonical_order1(a::Concrete{Complex{Rational{BigInt}}},b::Concrete{Complex{Rational{BigInt}}})
    if canonical_order1(Concrete(a.re),Concrete(b.re))
        return true
    end
    if canonical_order1(Concrete(b.re),Concrete(a.re))
        return false
    end
    return canonical_order1(Concrete(a.im),Concrete(b.im))
end


function canonical_order1(a::Application,b::Symbolic)
    if a.head isa Atom
        if a.head.name==:*
            return canonical_order1(a,Application(a.head,[b]))
        end
        if a.head.name==:^
            return canonical_order1(a,Application(a.head,[b,Concrete{Rational{BigInt}}(1)]))
        end
        if a.head.name==:+
            return canonical_order1(a,Application(a.head,[b]))
        end
    end
    return false
end

function canonical_order1(a::Symbolic,b::Application)
    if b.head isa Atom
        if b.head.name==:*
            return canonical_order1(Application(b.head,[a]),b)
        end
        if b.head.name==:^
            return canonical_order1(Application(b.head,[a,Concrete{Rational{BigInt}}(1)]),b)
        end
        if b.head.name==:+
            return canonical_order1(Application(b.head,[a]),b)
        end
    end
    return true
end

function canonical_order1(a::Curly,b::Curly)
    if a.head==b.head
        if length(a.args)==0
            return true
        end
        if length(b.args)==0
            return false
        end
        if a.args[end]==b.args[end]
            return canonical_order1(Curly(a.head,a.args[1:end-1]),Curly(b.head,b.args[1:end-1]))
        end
        return canonical_order1(a.args[end],b.args[end])
    end
    return canonical_order1(a.head,b.head)
end
        
function canonical_order1(a::Application,b::Application)
    if a.head==b.head
        if a.head isa Atom
            if a.head.name==:^
                if (length(a.args)==2)&&(length(b.args)==2)
                    if a.args[1]!=b.args[1]
                        return canonical_order1(a.args[1],b.args[1])
                    end
                    return canonical_order1(a.args[2],b.args[2])
                end
            end
        end
        
        if length(a.args)==0
            return true
        end
        if length(b.args)==0
            return false
        end
        if a.args[end]==b.args[end]
            return canonical_order1(Application(a.head,a.args[1:end-1]),Application(b.head,b.args[1:end-1]))
        end
        return canonical_order1(a.args[end],b.args[end])
    end

    if (a.head isa Atom)&&(a.head.name==:*)
        return canonical_order1(a,Application(a.head,[b]))
    end
    if (b.head isa Atom)&&(b.head.name==:*)
        return canonical_order1(Application(b.head,[a]),b)
    end
    
    if (a.head isa Atom)&&(a.head.name==:^)
        return canonical_order1(a,Application(a.head,[b,Concrete{Rational{BigInt}}(1)]))
    end
    if (b.head isa Atom)&&(b.head.name==:^)
        return canonical_order1(Application(b.head,[a,Concrete{Rational{BigInt}}(1)]),b)
    end

    if (a.head isa Atom)&&(a.head.name==:+)
        return canonical_order1(a,Application(a.head,[b]))
    end
    if (b.head isa Atom)&&(b.head.name==:+)
        return canonical_order1(Application(b.head,[a]),b)
    end
    
    return canonical_order1(a.head,b.head)
end

function canonical_order(a::Symbolic,b::Symbolic)
    sa=isSurdExpression(a,nothing)
    sb=isSurdExpression(b,nothing)
    if (sa)&&(!sb)
        return true
    end
    if (sb)&&(!sa)
        return false
    end
    return canonical_order1(a,b)
end


function canonical_sort(args::Vector{Symbolic},context,headsymbol)
    commuting=Symbolic[]
    noncommuting=Symbolic[]
    for a in args
        if (headsymbol in [:+,:∧,:∨,:(_assumptions),:bag])||
            ((headsymbol==:*)&&(commutesMultiplicatively(a,context)))
            push!(commuting,a)
        else
            push!(noncommuting,a)
        end
    end
    sort!(commuting,lt=canonical_order)
    return vcat(commuting,noncommuting)
end

struct Order <: Rewriter
end

function (r::Order)(s::Symbolic,context::Context)
    function results(cb)
    end
end

function (r::Order)(s::Application,context::Context)
    function results(cb)
        if !(s.head isa Atom)
            return
        end
        if !(s.head.name in [:+,:*,:∧,:∨,:bag])
            return
        end

        s1=Application(s.head,canonical_sort(s.args,context,s.head.name))
        if s1!=s
            @stopifdone cb(s1)
        end
    end
end

function (r::Order)(s::Curly,context::Context)
    function results(cb)
        if !(s.head isa Atom)
            return
        end
        if !(s.head.name in [:_assumptions])
            return
        end

        s1=Curly(s.head,canonical_sort(s.args,context,s.head.name))
        if s1!=s
            @stopifdone cb(s1)
        end
    end
end



end
