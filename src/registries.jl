module Registries

import ..Rewriter
import ..Symbolic

struct Registry
    rewriters::Dict{Symbol,Tuple{Int,Rewriter}}
    initialgroups::Dict{Symbol,Set{Symbol}}
    initialcontextvalues::Dict{Symbolic,Symbolic}
    incompatible::Dict{Symbol,Set{Symbol}}
end

Registry(initialgroups,
         initialcontextvalues)=Registry(Dict{Symbol,Tuple{Int,Rewriter}}(),
                                        initialgroups,
                                        initialcontextvalues,
                                        Dict{Symbol,Set{Symbol}}())

function register(reg::Registry,k::Symbol,r::Rewriter;priority=100,incompatible=Symbol[])
    reg.rewriters[k]=(priority,r)
    for x in incompatible
        if !haskey(reg.incompatible,k)
            reg.incompatible[k]=Set{Symbol}()
        end
        if !haskey(reg.incompatible,x)
            reg.incompatible[x]=Set{Symbol}()
        end
        push!(reg.incompatible[k],x)
        push!(reg.incompatible[x],k)
    end
end

end
