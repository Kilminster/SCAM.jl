module Contexts

import ..Rewriter
import ..Symbolic
import ..Symbolics
import ..Registries: Registry

struct GlobalContext
    registry::Registry
    noloop::Set{Symbolic}
    scratchpad::Dict{Any,IdDict{Any,Dict{Type,Any}}}
    newatomcnt::Ref{Int}
    newatommap::Dict{Symbolic,Int}
end

GlobalContext(registry::Registry)=GlobalContext(registry,
                                                Set{Symbolic}(),
                                                Dict{Any,IdDict{Any,Any}}(),
                                                Ref(0),
                                                Dict{Symbolic,Int}())

struct Context
    g::GlobalContext

    parent::Union{Context,Nothing}
    rewritergroups::Dict{Symbol,Set{Symbol}}
    values::Dict{Symbolic,Symbolic}

    scratchpad::IdDict{Any,Dict{Type,Any}}
end

function contextkey(parent,rewritergroups,values)
    if parent==nothing
        return (rewritergroups,values)
    end
    return (rewritergroups,values,contextkey(parent.parent,parent.rewritergroups,parent.values))
end

function Context(registry)
    rewritergroups=registry.initialgroups
    values=registry.initialcontextvalues
    k=contextkey(nothing,rewritergroups,values)
    g=GlobalContext(registry)
    g.scratchpad[k]=IdDict{Any,Any}()
    Context(g,nothing,rewritergroups,values,g.scratchpad[k])
end

function Context(parent,rewritergroups,values)
    k=contextkey(parent,rewritergroups,values)
    g=parent.g
    if !haskey(g.scratchpad,k)
        g.scratchpad[k]=IdDict{Any,Any}()
    end
    Context(g,parent,rewritergroups,values,g.scratchpad[k])
end

function scratchpad(c::Context,rewriter,::Type{T}) where T
    if !haskey(c.scratchpad,rewriter)
        c.scratchpad[rewriter]=Dict{Type,Any}()
    end
    if !haskey(c.scratchpad[rewriter],T)
        c.scratchpad[rewriter][T]=T()
    end
    return c.scratchpad[rewriter][T]::T
end

function newatom(c::Context,s::Symbolic,::Type{T}) where T
    if !haskey(c.g.newatommap,s)
        c.g.newatomcnt[]=c.g.newatomcnt[]+1
        c.g.newatommap[s]=c.g.newatomcnt[]
    end
    i=c.g.newatommap[s]
    return Symbolics.Atom{T}(Symbol("∎$(i)∎"))
end

function clear(c::Context)
    for k in keys(c.g.scratchpad)
        empty!(c.g.scratchpad[k])
    end
    c.g.newatomcnt[]=0
    empty!(c.g.newatommap)
end

end
