module GroebnerRules

import ..@symbolic
import ..@symbolic0
import ..Symbolics: Application
import ..Rewriters: FunctionWrap,Rule,RewriterList,Reduce,Group,NewAtom
import ..Rewriters
import ..Registries: register
import ..defaultregistry
import ..Groebner
import ..SymbolicFunctions: isConcretePositive

const checktrue=Rewriters.CheckTrue(Reduce(Group(:standard)))
const checkbool=Rewriters.CheckBool(Reduce(Group(:standard)))
const standard=Reduce(Group(:standard))

register(defaultregistry,:groebner,
         RewriterList([#
                       Rule((@symbolic _strict{groebner(F...)}),
                            (@symbolic R),:F,
                            transform=[:R=>(FunctionWrap((s,c)->
                                                         try
                                                         Application((@symbolic bag),Groebner.groebner(s.args))
                                                         catch
                                                         nothing
                                                         end),
                                            @symbolic groebner(F...))]),

                       Rule((@symbolic groebner()),
                            (@symbolic bag())),
                       
                       # _groebner_equality_assumptions

                       Rule((@symbolic _groebner_equality_assumptions()),
                            (@symbolic _groebner_equality_assumptions{}(contextvalue{assumptions}))),

                       Rule((@symbolic _groebner_equality_assumptions{P...}(_assumptions{X==Y,A...})),
                            (@symbolic _groebner_equality_assumptions{P...,F}(_assumptions{A...})),
                            :P,:X,:Y,:A,
                            transform=[:F=>(standard,@symbolic polyFreeze(X-Y))]),

                       Rule((@symbolic _groebner_equality_assumptions{P...}(_assumptions{A1,A...})),
                            (@symbolic _groebner_equality_assumptions{P...}(_assumptions{A...})),
                            :P,:X,:Y,:A,:A1),

                       Rule((@symbolic _groebner_equality_assumptions{P...}(_assumptions{})),
                            (@symbolic groebner(P...)),:P),

                       # _groebner_inequality_assumptions

                       Rule((@symbolic _groebner_inequality_assumptions()),
                            (@symbolic _groebner_inequality_assumptions{}(contextvalue{assumptions}))),

                       Rule((@symbolic _groebner_inequality_assumptions{P...}(_assumptions{X>Y,A...})),
                            (@symbolic _groebner_inequality_assumptions{P...,F}(_assumptions{A...})),
                            :P,:X,:Y,:A,
                            transform=[:N=>(NewAtom(),@symbolic _groebnerslack{X>Y}),
                                       :F=>(standard,@symbolic polyFreeze(X-Y-_slackP{N}))]),

                       Rule((@symbolic _groebner_inequality_assumptions{P...}(_assumptions{X<Y,A...})),
                            (@symbolic _groebner_inequality_assumptions{P...,F}(_assumptions{A...})),
                            :P,:X,:Y,:A,
                            transform=[:N=>(NewAtom(),@symbolic _groebnerslack{X<Y}),
                                       :F=>(standard,@symbolic polyFreeze(X-Y+_slackP{N}))]),
                       
                       Rule((@symbolic _groebner_inequality_assumptions{P...}(_assumptions{X>=Y,A...})),
                            (@symbolic _groebner_inequality_assumptions{P...,F}(_assumptions{A...})),
                            :P,:X,:Y,:A,
                            transform=[:N=>(NewAtom(),@symbolic _groebnerslack{X>=Y}),
                                       :F=>(standard,@symbolic polyFreeze(X-Y-_slackN{N}))]),

                       Rule((@symbolic _groebner_inequality_assumptions{P...}(_assumptions{X<=Y,A...})),
                            (@symbolic _groebner_inequality_assumptions{P...,F}(_assumptions{A...})),
                            :P,:X,:Y,:A,
                            transform=[:N=>(NewAtom(),@symbolic _groebnerslack{X<=Y}),
                                       :F=>(standard,@symbolic polyFreeze(X-Y+_slackN{N}))]),

                       Rule((@symbolic _groebner_inequality_assumptions{P...}(_assumptions{¬(X==Y),A...})),
                            (@symbolic _groebner_inequality_assumptions{P...,F}(_assumptions{A...})),
                            :P,:X,:Y,:A,
                            transform=[:N=>(NewAtom(),@symbolic _groebnerslack{X==Y}),
                                       :F=>(standard,@symbolic polyFreeze((X-Y)^2-_slackP{N}))]),
                       
                       Rule((@symbolic _groebner_inequality_assumptions{P...}(_assumptions{A1,A...})),
                            (@symbolic _groebner_inequality_assumptions{P...}(_assumptions{A...})),
                            :P,:X,:Y,:A,:A1),

                       Rule((@symbolic _groebner_inequality_assumptions{P...}(_assumptions{})),
                            (@symbolic groebner(P...)),:P),                       
                       
                       # Decide equality/inequality by groebner bases

                       Rule((@symbolic _groebner_decide(G,G1)),
                            (@symbolic false),
                            :G,:G1,
                            transform=[:_=>(checktrue,@symbolic G1===bag(1))]),

                       Rule((@symbolic _groebner_decide(G,G1)),
                            (@symbolic true),
                            :G,:G1,
                            transform=[:_=>(checktrue,@symbolic G===G1)]),

                       Rule((@symbolic _groebner_decide(G,bag(commute(P),R...))),
                            (@symbolic false),:G,:P,:R,
                            transform=[:_=>(checktrue,@symbolic _groebner_decide_isPositive(P))]),


#                       Rule((@symbolic _groebner_decide(G1,G2)),
#                            (@symbolic R),:G1,:G2,
#                            transform=[:R=>(FunctionWrap((s,c)->println(s)),@symbolic debug(G2))]),

                       
                       Rule((@symbolic _groebner_decide_isPositive(0)),
                            (@symbolic false)),

                       Rule((@symbolic _groebner_decide_isNonNegative(0)),
                            (@symbolic true)),

                       Rule((@symbolic _strict{_groebner_decide_isPositive(singletonwrap(+,+(X,Y...)))}),
                            (@symbolic _groebner_decide_isNonNegative(+(Y...))),:X,:Y,
                            transform=[:_=>(checktrue,@symbolic _groebner_decide_isPositive1(X))]),

                       Rule((@symbolic _strict{_groebner_decide_isPositive(singletonwrap(+,+(X,Y...)))}),
                            (@symbolic _groebner_decide_isPositive(+(Y...))),:X,:Y,
                            transform=[:_=>(checktrue,@symbolic _groebner_decide_isNonNegative1(X))]),

                       Rule((@symbolic _strict{_groebner_decide_isNonNegative(singletonwrap(+,+(X,Y...)))}),
                            (@symbolic _groebner_decide_isNonNegative(+(Y...))),:X,:Y,
                            transform=[:_=>(checktrue,@symbolic _groebner_decide_isNonNegative1(X))]),

                       Rule((@symbolic _groebner_decide_isPositive1(*(X,Y...))),
                            (@symbolic _groebner_decide_isPositive1(*(Y...))),:X,:Y,
                            transform=[:_=>(checktrue,@symbolic _groebner_decide_isPositive1(X))]),

                       Rule((@symbolic _groebner_decide_isNonNegative1(*(X,Y...))),
                            (@symbolic _groebner_decide_isNonNegative1(*(Y...))),:X,:Y,
                            transform=[:_=>(checktrue,@symbolic _groebner_decide_isNonNegative1(X))]),
                       
                       Rule((@symbolic _groebner_decide_isNonNegative1(*(commute(0),X...))),
                            (@symbolic true),:X),
                       
                       Rule((@symbolic _groebner_decide_isPositive1(N)),
                            (@symbolic true),:N=>isConcretePositive),
                             
                       Rule((@symbolic _groebner_decide_isNonNegative1(N)),
                            (@symbolic true),:N=>isConcretePositive),

                       Rule((@symbolic _groebner_decide_isPositive1(𝗫{_slackP{X}})),
                            (@symbolic true),:X),

                       Rule((@symbolic _groebner_decide_isNonNegative1(𝗫{_slackP{X}})),
                            (@symbolic true),:X),
                            
                       Rule((@symbolic _groebner_decide_isNonNegative1(𝗫{_slackN{X}})),
                            (@symbolic true),:X),

                       Rule((@symbolic _groebner_decide_isPositive1(𝗫{_slackP{X}}^N)),
                            (@symbolic true),:X,:N),

                       Rule((@symbolic _groebner_decide_isNonNegative1(𝗫{_slackP{X}}^N)),
                            (@symbolic true),:X,:N),
                            
                       Rule((@symbolic _groebner_decide_isNonNegative1(𝗫{_slackN{X}}^N)),
                            (@symbolic true),:X,:N),
                            
                       
                       Rule((@symbolic _groebnertest{A==0}),
                            (@symbolic R),
                            :A,:B,
                            transform=[:Ge=>(standard,@symbolic _groebner_equality_assumptions()),
                                       :Gi=>(standard,@symbolic _groebner_inequality_assumptions()),
                                       :F=>(Rule((@symbolic _{bag(T1...),bag(T2...)}),
                                                 (@symbolic groebner(T1...,T2...)),:T1,:T2),@symbolic _{Ge,Gi}),
                                       :G=>(standard,@symbolic F),
                                       :P=>(standard,@symbolic polyFreeze(A)),
                                       :F1=>(Rule((@symbolic _{bag(T...),P}),
                                                  (@symbolic groebner(T...,P)),:T,:P),@symbolic _{G,P}),
                                       :G1=>(standard,@symbolic F1),
                                       :R=>(checkbool,@symbolic _groebner_decide(G,G1))]),

                       Rule((@symbolic _groebnertest{A<0}),
                            (@symbolic R),
                            :A,:B,
                            transform=[:Ge=>(standard,@symbolic _groebner_equality_assumptions()),
                                       :Gi=>(standard,@symbolic _groebner_inequality_assumptions()),
                                       :F=>(Rule((@symbolic _{bag(T1...),bag(T2...)}),
                                                 (@symbolic groebner(T1...,T2...)),:T1,:T2),@symbolic _{Ge,Gi}),
                                       :G=>(standard,@symbolic F),
                                       :N=>(NewAtom(),@symbolic _groebnerslack{A<0}),
                                       :P=>(standard,@symbolic polyFreeze(A+_slackP{N})),
                                       :F1=>(Rule((@symbolic _{bag(T...),P}),
                                                  (@symbolic groebner(T...,P)),:T,:P),@symbolic _{G,P}),
                                       :G1=>(standard,@symbolic F1),
                                       :R=>(checkbool,@symbolic _groebner_decide(G,G1))]),

                       Rule((@symbolic _groebnertest{A<0}),
                            (@symbolic ¬R),
                            :A,:B,
                            transform=[:Ge=>(standard,@symbolic _groebner_equality_assumptions()),
                                       :Gi=>(standard,@symbolic _groebner_inequality_assumptions()),
                                       :F=>(Rule((@symbolic _{bag(T1...),bag(T2...)}),
                                                 (@symbolic groebner(T1...,T2...)),:T1,:T2),@symbolic _{Ge,Gi}),
                                       :G=>(standard,@symbolic F),
                                       :N=>(NewAtom(),@symbolic _groebnerslack{A<0}),
                                       :P=>(standard,@symbolic polyFreeze(A-_slackN{N})),
                                       :F1=>(Rule((@symbolic _{bag(T...),P}),
                                                  (@symbolic groebner(T...,P)),:T,:P),@symbolic _{G,P}),
                                       :G1=>(standard,@symbolic F1),
                                       :R=>(checkbool,@symbolic _groebner_decide(G,G1))]),

                       Rule((@symbolic _groebnertest{A<=0}),
                            (@symbolic R),
                            :A,:B,
                            transform=[:Ge=>(standard,@symbolic _groebner_equality_assumptions()),
                                       :Gi=>(standard,@symbolic _groebner_inequality_assumptions()),
                                       :F=>(Rule((@symbolic _{bag(T1...),bag(T2...)}),
                                                 (@symbolic groebner(T1...,T2...)),:T1,:T2),@symbolic _{Ge,Gi}),
                                       :G=>(standard,@symbolic F),
                                       :N=>(NewAtom(),@symbolic _groebnerslack{A<=0}),
                                       :P=>(standard,@symbolic polyFreeze(A+_slackN{N})),
                                       :F1=>(Rule((@symbolic _{bag(T...),P}),
                                                  (@symbolic groebner(T...,P)),:T,:P),@symbolic _{G,P}),
                                       :G1=>(standard,@symbolic F1),
                                       :R=>(checkbool,@symbolic _groebner_decide(G,G1))]),

                       Rule((@symbolic _groebnertest{A<=0}),
                            (@symbolic ¬R),
                            :A,:B,
                            transform=[:Ge=>(standard,@symbolic _groebner_equality_assumptions()),
                                       :Gi=>(standard,@symbolic _groebner_inequality_assumptions()),
                                       :F=>(Rule((@symbolic _{bag(T1...),bag(T2...)}),
                                                 (@symbolic groebner(T1...,T2...)),:T1,:T2),@symbolic _{Ge,Gi}),
                                       :G=>(standard,@symbolic F),
                                       :N=>(NewAtom(),@symbolic _groebnerslack{A<=0}),
                                       :P=>(standard,@symbolic polyFreeze(A-_slackP{N})),
                                       :F1=>(Rule((@symbolic _{bag(T...),P}),
                                                  (@symbolic groebner(T...,P)),:T,:P),@symbolic _{G,P}),
                                       :G1=>(standard,@symbolic F1),
                                       :R=>(checkbool,@symbolic _groebner_decide(G,G1))]),
                                                 
                       ]),
         priority=200)

                      


end
