module Rewriters

import ..Profile
import ..Rewriter
import ..Symbolic
import ..Symbolics: Application,Curly,Atom,Concrete
import ..Symbolics
import ..Pattern
import ..Patterns
import ..Patterns: makepattern
import ..Callbacks: @stopifdone,firstresult,Done,allresults
import ..Contexts: Context
import ..Contexts
import ..@symbolic
import ..@symbolic0

name(r)=typeof(r)

headhash(r::Rewriter)=UInt(3)

headhash(p::Pattern)=UInt(1)
function headhash(p::Patterns.ApplicationPattern)
    if p.head isa Patterns.LiteralAtom
        return hash(p.head.name)<<2+UInt(1)
    end
    return UInt(1)
end
headhash(p::Patterns.ArgPattern)=UInt(1)
headhash(p::Patterns.PlainArg)=headhash(p.p)
function headhash(p::Patterns.CurlyPattern)
    if p.head isa Patterns.LiteralAtom
        if p.head.name==:_strict
            return headhash(p.args[1])+UInt(1)
        end
        return hash(p.head.name)<<2+UInt(1)
    end
    return UInt(1)
end

headhash(s::Symbolic)=UInt(1)
function headhash(s::Application)
    if s.head isa Atom
        return hash(s.head.name)<<2+UInt(1)
    end
    return UInt(1)
end
function headhash(s::Curly)
    if s.head isa Atom
        if s.head.name==:_strict
            return headhash(s.args[1])+UInt(1)
        end
        return hash(s.head.name)<<2+UInt(1)
    end
    return UInt(1)
end

struct Rule <: Rewriter
    pattern::Pattern
    template::Symbolic
    transform::Vector{Pair{Symbol,Tuple{Rewriter,Symbolic}}}
    ruleguard::Function
    name::String
end

headhash(r::Rule)=headhash(r.pattern)

name(r::Rule)=r.name

Rule(pattern::Symbolic,
     template::Symbolic,
     vars...;
     transform=[],
     ruleguard=(bindings,context)->true)=Rule(makepattern(pattern,vars...),
                                              template,transform,ruleguard,"$(pattern) -> $(template)")

function applytransforms(bindings,transform,context)
    function results(cb)
        if length(transform)==0
            @stopifdone cb(bindings)
        else
            v=transform[1][1]
            r=transform[1][2][1]
            t=transform[1][2][2]
            s=replace(t,bindings)
            @stopifdone r(s,context)() do result
                bindings1=merge(bindings,Dict(v=>result))
                @stopifdone applytransforms(bindings1,transform[2:end],context)(cb)
            end
        end
    end
end

function (r::Rule)(s::Symbolic,context)
    function results(cb)
        @stopifdone r.pattern(s,Dict{Symbol,Any}(),context)() do bindings
            @stopifdone applytransforms(bindings,r.transform,context)() do bindings1
                if r.ruleguard(bindings1,context)
#                    println("Applying rule: $(r.name)")
                    @stopifdone cb(replace(r.template,bindings1))
                end
            end
        end
    end
end



struct RewriterList <: Rewriter
    d::Dict{UInt,Vector{Rewriter}}
    function RewriterList(l)
        d=Dict{UInt,Vector{Rewriter}}()
        K=Set{UInt}([UInt(1),UInt(2)])
        for r in l
            h=headhash(r)
            push!(K,h)
        end
        for k in K
            d[k]=Rewriter[]
            k1=k&UInt(1)
            k2=k&UInt(2)
            for r in l
                h=headhash(r)
                h1=h&UInt(1)
                h2=h&UInt(2)
                if (h==k)||(h1==k1)||(h2==k2)
                    push!(d[k],r)
                end
            end
        end
        new(d)
    end
end

function (r::RewriterList)(s::Symbolic,context)
    function results(cb)
        h=headhash(s)
        if !haskey(r.d,h)
            h=h&UInt(3)
        end
        for r1 in r.d[h]
            @static if Profile.PROFILE
                Profile.start(name(r1))
            end
            x=r1(s,context)(cb)
            @static if Profile.PROFILE
                Profile.stop()
            end
            if x isa Done
                return x
            end
        end
    end
end

struct Repeat <: Rewriter
    r::Rewriter
end

function (r::Repeat)(s::Symbolic,context::Context)
    function results(cb)
        @stopifdone r.r(s,context)() do s1
            flag=true
            @stopifdone r(s1,context)() do s2
                flag=false
                @stopifdone cb(s2)
            end
            if flag
                @stopifdone cb(s1)
            end
        end
    end
end

struct BagResults <: Rewriter
    r::Rewriter
end

function (r::BagResults)(s::Symbolic,context::Context)
    function results(cb)
        args=allresults(r.r(s,context))
        @stopifdone cb(Application((@symbolic bag),args))
    end
end


reducelevel=0

struct Reduce <: Rewriter
    r::Rewriter
end

function (r::Reduce)(s::Symbolic,context::Context)
    recurse(s)=nothing

    function recurse(s::Application)
        h1=firstresult(r(s.head,context))
        if h1!=nothing
            return Application(h1,s.args)
        end
        for i=1:length(s.args)
            a1=firstresult(r(s.args[i],context))
            if a1!=nothing
                return Application(s.head,vcat(s.args[1:i-1],a1,s.args[i+1:end]))
            end
        end
        return nothing
    end
    
    function recurse(s::Curly)
        h1=firstresult(r(s.head,context))
        if h1!=nothing
            return Curly(h1,s.args)
        end
        return nothing
    end
    
    function results(cb)
        global reducelevel
        reduced=Contexts.scratchpad(context,r,IdDict{Symbolic,Nothing})
        if haskey(reduced,s)
            return
        end
        cached=Contexts.scratchpad(context,r,Dict{Symbolic,Symbolic})
        if haskey(cached,s)
            @stopifdone cb(cached[s])
            return
        end
        cachekey=s
        noloop=Contexts.scratchpad(context,r,Set{Symbolic})
        if cachekey in noloop
            return
        end
        push!(noloop,cachekey)
        noloop2=Set{Symbolic}()
        flag=true
        changed=false
        #        println("### $(s) $(objectid(s))")
        reducelevel=reducelevel+1
        while flag
            if s in noloop2
                reducelevel=reducelevel-1
                reduced[s]=nothing
                delete!(noloop,cachekey)
                return
            end
            push!(noloop2,s)
#            println("*"^reducelevel*" $(s)")
            flag=false
            s1=firstresult(r.r(s,context))
            if s1!=nothing
                s=s1
                flag=true
                changed=true
            else
                s2=recurse(s)
                if s2!=nothing
                    s=s2
                    flag=true
                    changed=true
                else
                    s3=firstresult(r.r(Curly((@symbolic _strict),[s]),context))
                    if s3!=nothing
                        s=s3
                        flag=true
                        changed=true
                    end
                end
            end
        end
        reducelevel=reducelevel-1
        reduced[s]=nothing
        delete!(noloop,cachekey)
        if changed
            cached[cachekey]=s
            @stopifdone cb(s)
        end
    end
end


struct Group <: Rewriter
    name::Symbol
end

function (r::Group)(s::Symbolic,context::Context)
    function results(cb)
        R=Tuple{Int,Rewriter,Symbol}[]
        for n in context.rewritergroups[r.name]
            push!(R,(context.g.registry.rewriters[n]...,n))
        end
        sort!(R,by=x->x[1])
        for r in R
            @static if Profile.PROFILE
                Profile.start(r[3])
            end
            x=r[2](s,context)(cb)
            @static if Profile.PROFILE
                Profile.stop()
            end
            if x isa Done
                return x
            end
        end
    end
end


struct CheckTrue <: Rewriter
    r::Rewriter
end

function (r::CheckTrue)(s::Symbolic,context)
    function match(cb)
#        println("check: $s")
        if s==@symbolic true
            @stopifdone cb(x)
            return
        end
        @stopifdone r.r(s,context)() do x
            if x==@symbolic true
                @stopifdone cb(x)
            end
        end
    end
end


struct CheckBool <: Rewriter
    r::Rewriter
end

function (r::CheckBool)(s::Symbolic,context)
    function match(cb)
        if (s==@symbolic true)||(s==@symbolic false)
            @stopifdone cb(s)
            return
        end
        @stopifdone r.r(s,context)() do x
            if (x==@symbolic true)||(x==@symbolic false)
                @stopifdone cb(x)
            end
        end
    end
end


struct ContextValue <: Rewriter
end

function (r::ContextValue)(s::Symbolic,context)
    function results(cb)
    end
end

function (r::ContextValue)(s::Curly,context)
    function results(cb)
        if s.head isa Atom
            if s.head.name==:contextvalue
                if length(s.args)==1
                    if haskey(context.values,s.args[1])
                        @stopifdone cb(context.values[s.args[1]])
                    else
                        @stopifdone cb(@symbolic Undefined)
                    end
                end
            end
        end
    end
end





struct SubContext <: Rewriter
    pattern::Pattern
    template::Symbolic
    r::Rewriter
    newvalues::Symbolic
    groupadditions::Dict{Symbol,Set{Symbol}}
end

SubContext(pattern::Symbolic,
           template::Symbolic,
           r::Rewriter,
           vars...;
           newvalues=(@symbolic _newvalues{}),
           groupadditions=Dict{Symbol,Set{Symbol}}()
           )=SubContext(makepattern(pattern,vars...),
                        template,
                        r,
                        newvalues,
                        groupadditions)

function (r::SubContext)(s::Symbolic,context)
    function results(cb)
        @stopifdone r.pattern(s,Dict{Symbol,Any}(),context)() do bindings
            nv=(replace(r.newvalues,bindings)::Curly).args
            values=Dict{Symbolic,Symbolic}()
            while length(nv)>=2
                values[nv[1]]=nv[2]
                nv=nv[3:end]
            end
            values=merge(context.values,values)
            rewritergroups=Dict{Symbol,Set{Symbol}}()
            for (k,v) in r.groupadditions
                rewritergroups[k]=if haskey(context.rewritergroups,k)
                    copy(context.rewritergroups[k])
                else
                    Set{Symbol}()
                end
                for x in v
                    if haskey(context.g.registry.incompatible,x)
                        for s in context.g.registry.incompatible[x]
                            delete!(rewritergroups[k],s)
                        end
                    end
                    push!(rewritergroups[k],x)
                end
            end
            rewritergroups=merge(context.rewritergroups,rewritergroups)
            subcontext=Contexts.Context(context,rewritergroups,values)
            flag=true
            s1=replace(r.template,bindings)
            @stopifdone r.r(s1,subcontext)() do s2
                flag=false
                @stopifdone cb(s2)
            end
            if flag
                @stopifdone cb(s1)
            end
        end
    end
end



struct NoLoop <: Rewriter
    pattern::Pattern
    key::Symbolic
    template::Symbolic
    r::Rewriter
end


NoLoop(pattern::Symbolic,
       key::Symbolic,
       template::Symbolic,
       r::Rewriter,
       vars...)=NoLoop(makepattern(pattern,vars...),
                       key,
                       template,
                       r)

function (r::NoLoop)(s::Symbolic,context)
    function results(cb)
        @stopifdone r.pattern(s,Dict{Symbol,Any}(),context)() do bindings
            key=replace(r.key,bindings)
            if !(key in context.g.noloop)
                push!(context.g.noloop,key)
                a=r.r(replace(r.template,bindings),context)(cb)
                delete!(context.g.noloop,key)
                if a isa Done
                    return a
                end
            end
        end
    end
end


struct MustChange <: Rewriter
    r::Rewriter
end

function (r::MustChange)(s::Symbolic,context)
    function results(cb)
        @stopifdone r.r(s,context)() do s1
            if s1!=s
                @stopifdone cb(s1)
            end
        end
    end
end


struct Recursive <: Rewriter
    r::Rewriter
end

function (r::Recursive)(s::Symbolic,context)
    function results(cb)
        S=Contexts.scratchpad(context,r,Set{Symbolic})
        if s in S
            return
        end
        
        nochange=true
        
        @stopifdone r.r(s,context)() do x
            nochange=false
            @stopifdone cb(x)
        end
        
        if s isa Application
            @stopifdone r(s.head,context)() do head1
                nochange=false
                @stopifdone cb(Application(head1,s.args))
            end
            
            for i=1:length(s.args)
                @stopifdone r(s.args[i],context)() do arg1
                    nochange=false
                    @stopifdone cb(Application(s.head,vcat(s.args[1:i-1],arg1,s.args[i+1:end])))
                end
            end
        end
        
        if nochange
            push!(S,s)
        end
        
    end
end



struct BestFirst{SCORE} <: Rewriter
    deterministic::Rewriter
    nondeterministic::Rewriter
    score::SCORE
    returnable
    limit::Symbolic
end

function (r::BestFirst)(start::Symbolic,context)

    function deterministic(s)
        s1=firstresult((r.deterministic)(s,context))
        if s1==nothing
            return s
        end
        return s1
    end
    
    s=deterministic(start)
    best=(r.score(s),s)
    todo=Tuple{Int,Symbolic}[best]
    done=Set{Symbolic}()
    limit=try
        Int(context.values[r.limit].value)
    catch
        0
    end
    
    function results(cb)
        for i=1:limit
            if length(todo)==0
                break
            end
            sort!(todo,by=x->x[1])
#            println(todo[1]) # For Debugging
            s1=popfirst!(todo)[2]
            push!(done,s1)
            for s3 in allresults(r.nondeterministic(s1,context))
#                println("*-> $s3")
                s2=deterministic(s3)
#                println("--> $s2")
                if !(s2 in done)
                    current=(r.score(s2),s2)
                    push!(todo,current)
                    if current[1]<best[1]
                        best=current
                        if best[1]==0
                            if best[2]!=start
                                if r.returnable(best[2])
                                    @stopifdone cb(best[2])
                                end
                            end
                            return
                        end
                    end
                end
            end
        end
        if best[2]!=start
            if r.returnable(best[2])
                @stopifdone cb(best[2])
            end
        end
    end
end


struct NewAtom <: Rewriter
end

function (r::NewAtom)(s::Symbolic,context)
    function results(cb)
        @stopifdone cb(Contexts.newatom(context,s,Union{}))
    end
end


struct FunctionWrap{F} <: Rewriter
    f::F
end

function (r::FunctionWrap)(s::Symbolic,context)
    function results(cb)
        s1=r.f(s,context)
        if s1 isa Symbolic
            @stopifdone cb(s1)
        end
    end
end

end
