module Callbacks

struct Done{T}
    result::T
end

macro stopifdone(ex)
    quote
        r=$(esc(ex))
        if r isa Done
            return r
        end
    end
end

function allresults(bt)
    r=Any[]
    bt() do x
        push!(r,x)
    end
    return r
end

function firstresult(bt)
    r=bt() do x
        Done(x)
    end
    if r isa Done
        return r.result
    end
    return nothing
end

end

