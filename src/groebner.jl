module Groebner

import ..Symbolic
import ..Symbolics: Application,Atom,Curly,Concrete
import ..CanonicalOrder
import ..@symbolic
import ..@symbolic0
import ..Patterns: makepattern
import ..Callbacks: firstresult

struct Term{K,P}
    coefficient::K
    powers::Vector{P}
end

const Poly{K,P}=Vector{Term{K,P}}

function lexorder(t1::Term,t2::Term)
    for p in t1.powers-t2.powers
        if p>0
            return true
        end
        if p<0
            return false
        end
    end
    return false
end

function grlexorder(t1::Term,t2::Term)
    if sum(t1.powers)>sum(t2.powers)
        return true
    end
    if sum(t1.powers)<sum(t2.powers)
        return false
    end
    return lexorder(t1,t2)
end

function grevlexorder(t1::Term,t2::Term)
    if sum(t1.powers)>sum(t2.powers)
        return true
    end
    if sum(t1.powers)<sum(t2.powers)
        return false
    end
    for p in reverse(t1.powers.-t2.powers)
        if p<0
            return true
        end
        if p>0
            return false
        end
    end
    return false
end


function simplify(p::Poly,order)
    r=empty(p)
    sort!(p,lt=order)
    while length(p)>0
        t=popfirst!(p)
        if (length(r)>0)&&(r[end].powers==t.powers)
            r[end]=Term(r[end].coefficient+t.coefficient,r[end].powers)
        else
            push!(r,t)
        end
        if r[end].coefficient==0
            pop!(r)
        end
    end
    return r
end

function normalise(p::Poly,order)
    r=empty(p)
    for t in p
        push!(r,Term(t.coefficient/p[1].coefficient,t.powers))
    end
    return r
end


"""
    reduction(u::Poly,F::Vector{Poly},order)

Returns the reduction of `u` with respect to the set `F` and `order`.
The inputs should be `order`-sorted with no zero coefficients.
No element of `F` should be zero.
"""
function reduction(u::Poly{K,P},F::Vector{Poly{K,P}},order) where K where P
    R=copy(u)
    r=empty(u)
    i=1
    while length(R)>0
        if i>length(F)
            push!(r,popfirst!(R))
            i=1
        else
            pdiff=R[1].powers.-F[i][1].powers
            if all(pdiff.>=0)
                cratio=-R[1].coefficient/F[i][1].coefficient
                popfirst!(R)
                for ft in F[i][2:end]
                    push!(R,Term(ft.coefficient*cratio,ft.powers.+pdiff))
                end
                R=simplify(R,order)
                i=1
            else
                i=i+1
            end
        end
    end
    return simplify(r,order)
end

function spoly(f1::Poly,f2::Poly,order)
    r=empty(f1)
    maxpowers=max.(f1[1].powers,f2[1].powers)
    for t in f1[2:end]
        push!(r,Term(t.coefficient*f2[1].coefficient,t.powers.+maxpowers.-f1[1].powers))
    end
    for t in f2[2:end]
        push!(r,Term(-t.coefficient*f1[1].coefficient,t.powers.+maxpowers.-f2[1].powers))
    end
    return simplify(r,order)
end


function buchberger(F::Vector{Poly{K,P}},order) where K where P
    G=Poly{K,P}[normalise(simplify(f,order),order) for f in F]
    S=Poly{K,P}[]
    for i=1:length(G)
        for j=i+1:length(G)
            push!(S,spoly(G[i],G[j],order))
        end
    end
    while length(S)>0
        g=normalise(reduction(pop!(S),G,order),order)
        if length(g)>0
            for i=1:length(G)
                push!(S,spoly(G[i],g,order))
            end
            push!(G,g)
        end
    end
    R=Poly{K,P}[]
    while length(G)>0
        g=pop!(G)
        g1=reduction(g,vcat(R,G),order)
        if length(g1)>0
            push!(R,g1)
        end
    end
    return R
end

variables(::Symbolic)=Set{Symbolic}()
variables(s::Application)=union(variables(s.head),variables.(s.args)...)
variables(s::Curly)=Set{Symbolic}([s])

function maketerm(f::Symbolic,v)
    powers=zeros(BigInt,length(v))
    coeff=Complex{Rational{BigInt}}(1)
    pat=makepattern((@symbolic singletonwrap(*,*(X...))),:X)
    for part in firstresult(pat(f,Dict{Symbol,Any}(),nothing))[:X]
        if part isa Concrete
            coeff=coeff*part.value
        else
            if part in v
                i=indexin([part],v)[1]
                powers[i]=powers[i]+1
            else
                if part.args[1] in v
                    i=indexin([part.args[1]],v)[1]
                    powers[i]=powers[i]+BigInt(part.args[2].value)
                end
            end
        end
    end
    return Term(coeff,powers)
end

function makepoly(f::Symbolic,v)
    pat=makepattern((@symbolic singletonwrap(+,+(X...))),:X)
    terms=firstresult(pat(f,Dict{Symbol,Any}(),nothing))[:X]
    return [maketerm(t,v) for t in terms]
end

function groebner(F::Vector{Symbolic},order=grevlexorder)
    v=sort(collect(union(variables.(F)...)),lt=CanonicalOrder.canonical_order)
    G=buchberger([makepoly(f,v) for f in F],order)
    R=Symbolic[]
    for g in G
        g1=Symbolic[]
        for t in g
            t1=Symbolic[]
            push!(t1,Concrete(t.coefficient))
            for i=1:length(v)
                if t.powers[i]>0
                    push!(t1,Application((@symbolic ^),[v[i],Concrete(Rational(t.powers[i]))]))
                end
            end
            push!(g1,Application((@symbolic *),t1))
        end
        push!(R,Application((@symbolic +),g1))
    end
    return R
end




end
