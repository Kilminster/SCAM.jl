module Patterns

import ..Symbolics: Symbolic,Atom,Concrete,Application,Curly
import ..Callbacks: @stopifdone
import ..Pattern

abstract type ArgPattern end

struct LiteralAtom{T} <: Pattern
    name::Symbol
end

function (::LiteralAtom)(s::Symbolic,bindings,context)
    function matches(cb)
    end
end

function (l::LiteralAtom{T})(s::Atom{T1},bindings,context) where T1 <: T where T
    function matches(cb)
        if s.name==l.name
            @stopifdone cb(bindings)
        end
    end
end


struct ConcretePattern{T} <: Pattern
    value::T
end

function (c::ConcretePattern)(s::Symbolic,bindings,context)
    function match(cb)
    end
end

function (c::ConcretePattern)(s::Concrete,bindings,context)
    function match(cb)
        if s.value==c.value
            @stopifdone cb(bindings)
        end
    end
end


struct Variable <: Pattern
    name::Symbol
    guard
end

function (v::Variable)(s::Symbolic,bindings,context)
    function matches(cb)
        if haskey(bindings,v.name)
            if bindings[v.name]==s
                @stopifdone cb(bindings)
            end
        else
            if v.guard(s,context)
                @stopifdone cb(merge(bindings,Dict(v.name=>s)))
            end
        end
    end
end


struct ApplicationPattern <: Pattern
    head::Pattern
    args::Vector{ArgPattern}
end

function (a::ApplicationPattern)(s::Symbolic,bindings,context)
    function match(cb)
    end
end

function (a::ApplicationPattern)(s::Application,bindings,context)
    function match(cb)
        @stopifdone (a.head)(s.head,bindings,context)() do bindings1
            @stopifdone argmatches(a.args,s.args,bindings1,context)(cb)
        end
    end
end


struct CurlyPattern <: Pattern
    head::Pattern
    args::Vector{ArgPattern}
end

function (c::CurlyPattern)(s::Symbolic,bindings,context)
    function match(cb)
    end
end

function (c::CurlyPattern)(s::Curly,bindings,context)
    function match(cb)
        @stopifdone (c.head)(s.head,bindings,context)() do bindings1
            @stopifdone argmatches(c.args,s.args,bindings1,context)(cb)
        end
    end
end



function argmatches(p::Vector{ArgPattern},s::Vector{Symbolic},bindings,context)
    function match(cb)
        if length(p)==0
            if length(s)==0
                @stopifdone cb(bindings)
            end
            return
        end
        @stopifdone argmatches(p[1],p[2:end],s,bindings,context)(cb)
    end
end


struct PlainArg <: ArgPattern
    p::Pattern
end

function argmatches(p::PlainArg,rest::Vector{ArgPattern},s::Vector{Symbolic},bindings,context)
    function match(cb)
        if length(s)>0
            @stopifdone (p.p)(s[1],bindings,context)() do bindings1
                @stopifdone argmatches(rest,s[2:end],bindings1,context)(cb)
            end
        end
    end
end




struct CommutingArg <: ArgPattern
    p::Pattern
end

function argmatches(p::CommutingArg,rest::Vector{ArgPattern},s::Vector{Symbolic},bindings,context)
    function match(cb)
        for pos=1:length(s)
            @stopifdone (p.p)(s[pos],bindings,context)() do bindings1
                @stopifdone argmatches(rest,vcat(s[1:pos-1],s[pos+1:end]),bindings1,context)(cb)
            end
        end
    end
end


struct SlurpVariable <: ArgPattern
    name::Symbol
    guard
end

function SlurpVariable(s::Atom,variables,variableguards)
    name=s.name
    @assert(name in variables)
    guard=if haskey(variableguards,name)
        variableguards[name]
    else
        (x,context)->true
    end
    return SlurpVariable(name,guard)
end

function argmatches(p::SlurpVariable,rest::Vector{ArgPattern},s::Vector{Symbolic},bindings,context)
    function match(cb)
        if haskey(bindings,p.name)
            k=length(bindings[p.name])
            if (length(s)>=k)&&(s[1:k]==bindings[p.name])
                @stopifdone argmatches(rest,s[k+1:end],bindings,context)(cb)
            end
        else
            for k=0:length(s)
                if p.guard(s[1:k],context)
                    @stopifdone argmatches(rest,s[k+1:end],merge(bindings,Dict(p.name=>s[1:k])),context)(cb)
                end
            end
        end
    end
end


struct SingletonWrap <: Pattern
    head::Symbolic
    p::Pattern
end

function (p::SingletonWrap)(s::Symbolic,bindings,context)
    function match(cb)
        if (s isa Application)&&(s.head==p.head)
            @stopifdone p.p(s,bindings,context)(cb)
        else
            @stopifdone p.p(Application(p.head,[s]),bindings,context)(cb)
        end
    end
end


function makepattern1(s::Atom{T},variables,variableguards) where T
    if s.name in variables
        guard=if haskey(variableguards,s.name)
            variableguards[s.name]
        else
            (s,context)->true
        end
        Variable(s.name,guard)
    else
        if T==Union{}
            LiteralAtom{Any}(s.name)
        else
            LiteralAtom{T}(s.name)
        end
    end
end

function makepattern1(s::Concrete,variables,variableguards)
    ConcretePattern(s.value)
end

function makepattern1(s::Application,variables,variableguards)
    if s.head isa Atom
        if s.head.name==:singletonwrap
            return SingletonWrap(s.args[1],makepattern1(s.args[2],variables,variableguards))
        end
    end
    head=makepattern1(s.head,variables,variableguards)
    args=ArgPattern[]
    for a in s.args
        if a isa Application
            if a.head isa Atom
                if a.head.name==:commute
                    push!(args,CommutingArg(makepattern1(a.args[1],variables,variableguards)))
                    continue
                end
                if a.head.name==:...
                    push!(args,SlurpVariable(a.args[1],variables,variableguards))
                    continue
                end
            end
        end
        push!(args,PlainArg(makepattern1(a,variables,variableguards)))
    end
    return ApplicationPattern(head,args)
end

function makepattern1(s::Curly,variables,variableguards)
    head=makepattern1(s.head,variables,variableguards)
    args=ArgPattern[]
    for a in s.args
        if a isa Application
            if a.head isa Atom
                if a.head.name==:commute
                    push!(args,CommutingArg(makepattern1(a.args[1],variables,variableguards)))
                    continue
                end
                if a.head.name==:...
                    if a.args[1] isa Application
                        if a.args[1].head isa Atom
                            if a.args[1].head.name==:commute
                                push!(args,CommutingSlurpVariable(a.args[1].args[1],variables,variableguards))
                                continue
                            end
                        end
                    end
                    push!(args,SlurpVariable(a.args[1],variables,variableguards))
                    continue
                end
            end
        end
        push!(args,PlainArg(makepattern1(a,variables,variableguards)))
    end
    return CurlyPattern(head,args)    
end


function makepattern(s::Symbolic,vars...)
    variables=Set{Symbol}()
    variableguards=Dict{Symbol,Any}()
    for v in vars
        if v isa Pair
            push!(variables,v[1])
            variableguards[v[1]]=v[2]
        else
            push!(variables,v)
        end
    end
    makepattern1(s,variables,variableguards)
end

end

