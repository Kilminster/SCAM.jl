module FunctionFacts

import ..Rewriter
import ..Rewriters: Rule,RewriterList,CheckTrue,Group,Reduce
import ..@symbolic
import ..@symbolic0
import ..defaultregistry
import ..Registries: register

# :d - derivative
# :ccts - continuous on complex numbers
# :cdfn - defined on all complex numbers
# :generic - f(x) is generic if x is.
# :nonzero - function returning true for arguments not mapped to zero

const facts=Dict(:sin=>[:d=>(@symbolic cos),
                        :ccts,:generic],
                 :cos=>[:d=>(@symbolic λ{-sin(x),x}),
                        :ccts,:generic],
                 :tan=>[:d=>(@symbolic λ{1/cos(x)^2,x}),
                        :generic],
                 :cosh=>[:d=>(@symbolic sinh),
                         :ccts,:generic],
                 :sinh=>[:d=>(@symbolic cosh),
                         :ccts,:generic],
                 :tanh=>[:d=>(@symbolic λ{1/cosh(x)^2,x}),
                         :generic],
                 :exp=>[:d=>(@symbolic exp),
                        :nonzero=>(@symbolic λ{true,x}),
                        :ccts,:generic],
                 :log=>[:d=>(@symbolic λ{1/x,x}),
                        :nonzero=>(@symbolic λ{¬(x==1),x}),
                        :generic],
                 :√=>[:d=>(@symbolic λ{1/2/√(x),x}),
                      :nonzero=>(@symbolic λ{¬(x==0),x}),
                      :generic],
                 :asin=>[:d=>(@symbolic λ{1/(1-x^2)^(1/2),x}),
                         :ccts,:generic],
                 :acos=>[:d=>(@symbolic λ{-1/(1-x^2)^(1/2),x}),
                         :ccts,:generic],
                 :atan=>[:d=>(@symbolic λ{1/(1+x^2),x}),
                         :ccts,:generic],
                 :asinh=>[:d=>(@symbolic λ{1/(x^2+1)^(1/2),x}),
                          :generic,:cdfn],
                 :acosh=>[:d=>(@symbolic λ{1/(x^2-1)^(1/2),x}),
                         :generic,:cdfn],
                 :atanh=>[:d=>(@symbolic λ{1/(1-x^2),x}),
                         :generic],
                 :erf=>[:d=>(@symbolic λ{2/(π)^(1/2)*exp(-x^2),x}),
                        :ccts,:generic],
                 :erfi=>[:d=>(@symbolic λ{2/(π)^(1/2)*exp(x^2),x}),
                         :ccts,:generic],
                 )

const checktrue=CheckTrue(Reduce(Group(:standard)))


function makerules()
    global facts
    R=Rewriter[]
    for fn in keys(facts)
        for fact in facts[fn]
            if fact isa Pair
                if fact[1]==:d
                    push!(R,Rule((@symbolic 𝔻{$(fn),1}),
                                 fact[2]))
                end
                if fact[1]==:nonzero
                    nz=fact[2]
                    push!(R,Rule((@symbolic $(fn)(X)==0),
                                 (@symbolic false),:X,
                                 transform=[:_=>(checktrue,@symbolic $(nz)(X))]))
                end
            end
            if fact in [:ccts,:cdfn]
                push!(R,Rule((@symbolic isFinite($(fn)(X))),
                             (@symbolic isFinite(X)),:X))
            end
            if fact in [:ccts]
                push!(R,Rule((@symbolic isContinuous{X}($(fn)(F))),
                             (@symbolic true),
                             :X,:F,
                             transform=[:_=>(checktrue,@symbolic isContinuous{X}(F))]))
            end
            if fact==:generic
                push!(R,Rule((@symbolic isGeneric($(fn)(F),X)),
                             (@symbolic isGeneric(F,X)),:F,:X))
            end                
        end
    end

    return RewriterList(R)
end

register(defaultregistry,:functionfacts,makerules())

end
