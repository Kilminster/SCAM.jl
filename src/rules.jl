module Rules

import ..@symbolic
import ..@symbolic0
import ..Rewriters
import ..Rewriters: Rule,RewriterList,Group,Reduce,NoLoop,MustChange,ContextValue,SubContext,CheckBool,FunctionWrap
import ..Rewriters: Repeat,BagResults
import ..Groebner
import ..CanonicalOrder
import ..defaultregistry
import ..Registries: register
import ..SymbolicFunctions: isConcreteNegative,isConcretePositive,isConcreteNumber,isConcreteInteger,isSurdExpression
import ..SymbolicFunctions: isConcreteRational
import ..ConcreteArithmetic
import ..Symbolics: Atom,Concrete,Application,Curly
import ..SpecialRewriters
import ..Integrals
import ..Trigvalues
import ..Callbacks
import ..SurdExpressions

const checktrue=Rewriters.CheckTrue(Reduce(Group(:standard)))
const checkbool=Rewriters.CheckBool(Reduce(Group(:standard)))
const standard=Reduce(Group(:standard))

register(defaultregistry,:basic,
         RewriterList([Rule((@symbolic _strict{F}),
                            (@symbolic R),:F,
                            transform=[:R=>(CanonicalOrder.Order(),@symbolic F)]),

                       # Concrete Arithmetic
                       Rule((@symbolic A^B),(@symbolic C),:A=>isConcreteNumber,:B=>isConcreteNumber,
                            transform=[:C=>(ConcreteArithmetic.Pair(),(@symbolic A^B))]),
                       Rule((@symbolic +(X...,A,B,Y...)),
                            (@symbolic +(X...,C,Y...)),
                            :X,:Y,:A=>isConcreteNumber,:B=>isConcreteNumber,
                            transform=[:C=>(ConcreteArithmetic.Pair(),(@symbolic +(A,B)))]),
                       Rule((@symbolic *(X...,A,B,Y...)),
                            (@symbolic *(X...,C,Y...)),
                            :X,:Y,:A=>isConcreteNumber,:B=>isConcreteNumber,
                            transform=[:C=>(ConcreteArithmetic.Pair(),(@symbolic *(A,B)))]),
                       Rule((@symbolic A==B),
                            (@symbolic C),
                            :A,:B,
                            transform=[:C=>(ConcreteArithmetic.Pair(),(@symbolic ==(A,B)))]),
                       Rule((@symbolic A>B),
                            (@symbolic C),
                            :A,:B,
                            transform=[:C=>(ConcreteArithmetic.Pair(),(@symbolic >(A,B)))]),
                       Rule((@symbolic A<B),
                            (@symbolic C),
                            :A,:B,
                            transform=[:C=>(ConcreteArithmetic.Pair(),(@symbolic <(A,B)))]),
                       Rule((@symbolic A>=B),
                            (@symbolic C),
                            :A,:B,
                            transform=[:C=>(ConcreteArithmetic.Pair(),(@symbolic >=(A,B)))]),
                       Rule((@symbolic A<=B),
                            (@symbolic C),
                            :A,:B,
                            transform=[:C=>(ConcreteArithmetic.Pair(),(@symbolic <=(A,B)))]),

                       
                       # +
                       Rule((@symbolic +(X...,+(Y...),Z...)),
                            (@symbolic +(X...,Y...,Z...)),
                            :X,:Y,:Z),
                       Rule((@symbolic +()),(@symbolic 0)),
                       Rule((@symbolic +(X)),(@symbolic X),:X),
                       Rule((@symbolic +(commute(0),X...)),(@symbolic +(X...)),:X),
                       Rule((@symbolic +(X...,singletonwrap(*,*(C1...,Y1,Y...)),singletonwrap(*,*(C2...,Y1,Y...)),Z...)),
                            (@symbolic +(X...,*(*(C1...)+*(C2...),Y1,Y...),Z...)),
                            :X,:Y1=>(s,c)->!isSurdExpression(s,c),
                            :Y,:Z,:C1=>isSurdExpression,:C2=>isSurdExpression),
                       Rule((@symbolic +(X...,singletonwrap(*,*(C1...,Y1,Y...)),singletonwrap(*,*(C2...,Y1,Y...)),Z...)),
                            (@symbolic +(X...,*(*(C1...)+*(C2...),Y1,Y...),Z...)),
                            :X,:Z,
                            :C1=>isConcreteNumber,
                            :C2=>isConcreteNumber,
                            :Y1=>isSurdExpression,
                            :Y=>isSurdExpression),
                            
                       
                       # *
                       Rule((@symbolic *(X...,*(Y...),Z...)),
                            (@symbolic *(X...,Y...,Z...)),
                            :X,:Y,:Z),
                       Rule((@symbolic *()),
                            (@symbolic 1)),
                       Rule((@symbolic *(X)),
                            (@symbolic X),:X),
                       Rule((@symbolic *(commute(1),X...)),
                            (@symbolic *(X...)),:X),
                       Rule((@symbolic *(X...,Y,Y,Z...)),
                            (@symbolic *(X...,Y^2,Z...)),
                            :X,:Y,:Z),
                       Rule((@symbolic *(X...,Y,Y^N,Z...)),
                            (@symbolic *(X...,Y^(N+1),Z...)),
                            :X,:Y=>(s,c)->!isConcretePositive(s,c),
                            :Z,:N=>isConcretePositive),
                       Rule((@symbolic _strict{*(X...,Y^M,Y^N,Z...)}),
                            (@symbolic *(X...,Y^(M+N),Z...)),
                            :X,:Y,:Z,:N=>isConcretePositive,:M=>isConcretePositive),

                       # ^
                       Rule((@symbolic (X^Y)^Z),(@symbolic X^(Y*Z)),:X,:Y=>isConcreteNumber,:Z=>isConcreteInteger),
                       Rule((@symbolic (*(X,Y...))^N),
                            (@symbolic X^N*(*(Y...))^N),
                            :X,:Y,:N=>isConcreteInteger),
                       Rule((@symbolic 0^N),(@symbolic Undefined),:N=>isConcreteNegative),
                       Rule((@symbolic 0^N),(@symbolic 0),:N=>isConcretePositive),
                       Rule((@symbolic X^1),(@symbolic X),:X),
                       Rule((@symbolic X^0),(@symbolic 1),:X),
                       Rule((@symbolic 1^X),(@symbolic 1),:X),
                       
                       # Boolean Stuff
                       Rule((@symbolic IF(true,A,B)),
                            (@symbolic A),:A,:B),
                       Rule((@symbolic IF(false,A,B)),
                            (@symbolic B),:A,:B),
                       Rule((@symbolic X==X),
                            (@symbolic true),:X),
                       Rule((@symbolic ¬(false)),(@symbolic true)),
                       Rule((@symbolic ¬(true)),(@symbolic false)),
                       Rule((@symbolic ∧(X...,∧(Y...),Z...)),
                            (@symbolic ∧(X...,Y...,Z...)),
                            :X,:Y,:Z),
                       Rule((@symbolic ∨(X...,∨(Y...),Z...)),
                            (@symbolic ∨(X...,Y...,Z...)),
                            :X,:Y,:Z),                      
                       Rule((@symbolic ∧(X)),(@symbolic X),:X),
                       Rule((@symbolic ∨(X)),(@symbolic X),:X),
                       Rule((@symbolic ∧()),(@symbolic true)),
                       Rule((@symbolic ∨()),(@symbolic false)),
                       Rule((@symbolic ∧(commute(true),X...)),
                            (@symbolic ∧(X...)),:X),
                       Rule((@symbolic ∧(commute(false),X...)),
                            (@symbolic false),:X),
                       Rule((@symbolic ∨(commute(false),X...)),
                            (@symbolic ∨(X...)),:X),
                       Rule((@symbolic ∨(commute(true),X...)),
                            (@symbolic true),:X),
                       Rule((@symbolic ∧(X...,Y,Y,Z...)),
                            (@symbolic ∧(X...,Y,Z...)),:X,:Y,:Z),
                       Rule((@symbolic ∨(X...,Y,Y,Z...)),
                            (@symbolic ∨(X...,Y,Z...)),:X,:Y,:Z),
                       ]),priority=0)

register(defaultregistry,:canonical,
         RewriterList([# -
                       Rule((@symbolic -X),(@symbolic (-1)*X),:X),
                       Rule((@symbolic X-(+(Y,Z...))),
                            (@symbolic X+(-1)*Y-(+(Z...))),
                            :X,:Y,:Z),
                       Rule((@symbolic X-Y),(@symbolic X+(-1)*Y),:X,:Y),
                       
                       # /
                       Rule((@symbolic X/(*(Y,Z...))),
                            (@symbolic X*Y^(-1)/(*(Z...))),
                            :X,:Y,:Z),
                       Rule((@symbolic X/Y),(@symbolic X*Y^(-1)),:X,:Y),

                       # Square Roots
                       Rule((@symbolic sqrt(X)),
                            (@symbolic X^(1/2)),:X),

                       Rule((@symbolic √(X)),
                            (@symbolic X^(1/2)),:X),

                       # pi

                       Rule((@symbolic pi),
                            (@symbolic π)),
                       ]))


register(defaultregistry,:pretty,
         RewriterList([# combine surds

                       Rule((@symbolic *(X...,A^P,Y...,B^P,Z...)),
                            (@symbolic *(X...,(A*B)^P,Y...,Z...)),
                            :X,:Y,:Z,
                            :A=>isConcretePositive,
                            :B=>isConcretePositive,
                            :P=>isConcreteRational),
                       
                       # /
                       Rule((@symbolic X^N),
                            (@symbolic 1/(X^M)),
                            :X,:N=>isConcreteNegative,
                            transform=[:M=>(ConcreteArithmetic.Pair(),@symbolic (-1)*N)]),
                       
                       Rule((@symbolic *(commute(W/X),Y...)),
                            (@symbolic *(W,Y...)/X),
                            :W,:X,:Y),
                       
                       Rule((@symbolic (X/Y)/Z),
                            (@symbolic X/(Z*Y)),
                            :X,:Y,:Z),
                       
                       # -
                       Rule((@symbolic *(N,X...)),
                            (@symbolic -(*(M,X...))),
                            :X,:N=>isConcreteNegative,
                            transform=[:M=>(ConcreteArithmetic.Pair(),@symbolic (-1)*N)]),
                       
                       Rule((@symbolic +(X,X1...,-Y,Z...)),
                            (@symbolic +(X,X1...)-Y+(+(Z...))),
                            :X,:X1,:Y,:Z),

                       # Square roots
                       Rule((@symbolic X^N),
                            (@symbolic √(X)),:X,:N,
                            transform=[:_=>(checktrue,@symbolic N==1/2)]),
                                            
                       ]))


register(defaultregistry,:basicfunctions,
         RewriterList([# denominator/numerator

                       Rule((@symbolic denominator(N)),
                            (@symbolic R),
                            :N=>(s,c)->s isa Concrete{Rational{BigInt}},
                            transform=[:R=>(FunctionWrap((s,c)->Concrete{Rational{BigInt}}(s.value.den)),@symbolic N)]),

                       Rule((@symbolic numerator(N)),
                            (@symbolic R),
                            :N=>(s,c)->s isa Concrete{Rational{BigInt}},
                            transform=[:R=>(FunctionWrap((s,c)->Concrete{Rational{BigInt}}(s.value.num)),@symbolic N)]),

                       Rule((@symbolic _splitterms2{X...}),
                            (@symbolic R),
                            :X,
                            transform=[:R=>(BagResults(Repeat(
                                RewriterList([
                                    Rule((@symbolic _splitterms{A...}{B...}{X1,X...}),
                                         (@symbolic _splitterms{A...,X1}{B...}{X...}),
                                         :A,:B,:X1,:X),
                                    Rule((@symbolic _splitterms{A...}{B...}{X1,X...}),
                                         (@symbolic _splitterms{A...}{B...,X1}{X...}),
                                         :A,:B,:X1,:X),
                                    Rule((@symbolic _splitterms{A...}{B...}{}),
                                         (@symbolic _{A...}{B...}),
                                         :A,:B),
                                ])
                            )),
                                            @symbolic _splitterms{}{}{X...})]),
                       
                       ]))



register(defaultregistry,:constructassumptions,
         RewriterList([Rule((@symbolic assume{F,A...}),
                            (@symbolic _assume{F,A...}{V}),:F,:A,
                            transform=[:V=>(standard,@symbolic contextvalue{assumptions})]),
                       Rule((@symbolic _assume{F,A1,A2...}{_assumptions{commute(A1),A...}}),
                            (@symbolic _assume{F,A2...}{_assumptions{A1,A...}}),:F,:A1,:A2,:A),
                       Rule((@symbolic _assume{F,A1,A2...}{_assumptions{A...}}),
                            (@symbolic _assume{F,A2...}{_assumptions{A1,A...}}),:F,:A1,:A2,:A),
                       Rule((@symbolic _assume{F}{A}),
                            (@symbolic context{F,assumptions,A1}),:F,:A,
                            transform=[:A1=>(standard,@symbolic _id(A))]),
                       ]))

register(defaultregistry,:contexts,
         RewriterList([ContextValue(),
                       SubContext((@symbolic context{F,X...}),
                                  (@symbolic F),
                                  standard,
                                  :F,:X,
                                  newvalues=(@symbolic _newvalues{X...})),
                      
                       ]))


register(defaultregistry,:predicates,
         RewriterList([# All assumptions are true
                       Rule((@symbolic F),
                            (@symbolic true),:F,
                            transform=[:A=>(ContextValue(),@symbolic contextvalue{assumptions}),
                                       :_=>(Rule((@symbolic _test{F,_assumptions{commute(F),R...}}),
                                                 (@symbolic true),:F,:R),@symbolic _test{F,A})]),
                       
                       # isFinite
                       Rule((@symbolic isFinite(Undefined)),(@symbolic false)),
                       Rule((@symbolic isFinite(∞)),(@symbolic false)),

                       Rule((@symbolic isFinite(X)),(@symbolic true),
                            :X=>(s,c)->(s isa Atom)||(s isa Concrete)),

                       Rule((@symbolic isFinite(+(X,Y...))),
                            (@symbolic isFinite(X)∧isFinite(+(Y...))),:X,:Y),

                       Rule((@symbolic isFinite(*(X,Y...))),
                            (@symbolic isFinite(X)∧isFinite(*(Y...))),:X,:Y),

                       Rule((@symbolic isFinite(X^Y)),
                            (@symbolic isFinite(X)∧isFinite(Y)∧((¬(X==0))∨(¬(Y<0)))),:X,:Y),

                       Rule((@symbolic isFinite(log(*(A1,A2...)))),
                            (@symbolic isFinite(log(A1))∧isFinite(log(*(A2...)))),:A1,:A2),
                       
                       # My justification for the following is that even though the result
                       # depends upon the chosen branch-cut, there are sensible answers
                       # in the complex plane.
                       
                       Rule((@symbolic isFinite(log(X))),
                            (@symbolic ¬(X==0)∧isFinite(X)),:X),

                       Rule((@symbolic isFinite(√(X))),
                            (@symbolic isFinite(X)),:X),

                       # isInteger

                       Rule((@symbolic isInteger(N)),
                            (@symbolic true),
                            :N=>isConcreteInteger),
                       Rule((@symbolic isInteger(N)),
                            (@symbolic false),
                            :N=>isConcreteNumber),
                       
                       # isFree
                       Rule((@symbolic isFree{X,X}),
                            (@symbolic false),
                            :X,:X),
                       Rule((@symbolic isFree{F,X}),
                            (@symbolic true),
                            :F=>(s,c)->(s isa Atom)||(s isa Concrete),
                            :X),
                       Rule((@symbolic isFree{F(Y,Y1...),X}),
                            (@symbolic isFree{Y,X}∧isFree{F(Y1...),X}),
                            :F,:Y,:Y1,:X),
                       Rule((@symbolic isFree{F(),X}),
                            (@symbolic isFree{F,X}),
                            :F,:X),
                       Rule((@symbolic isFree{𝗫{X},𝗫}),
                            (@symbolic false),:X),


                       # isSum
                       Rule((@symbolic isSum{+(A...)}),
                            (@symbolic true),:A),

                       Rule((@symbolic isSum{X}),
                            (@symbolic false),:X),

                       # isRational
                       Rule((@symbolic isRational(X)),
                            (@symbolic true),:X=>isConcreteRational),
                       
                       # isPolynomial
                       Rule((@symbolic isPolynomial{X}(C)),
                            (@symbolic true),:X,:C,
                            transform=[:_=>(checktrue,@symbolic isFree{C,X})]),

                       Rule((@symbolic isPolynomial{X}(X)),
                            (@symbolic true),:X),
                       
                       Rule((@symbolic isPolynomial{X}(+(F1,F...))),
                            (@symbolic isPolynomial{X}(F1)∧isPolynomial{X}(+(F...))),
                            :X,:F1,:F),
                       
                       Rule((@symbolic isPolynomial{X}(*(F1,F...))),
                            (@symbolic isPolynomial{X}(F1)∧isPolynomial{X}(*(F...))),
                            :X,:F1,:F),
                       
                       Rule((@symbolic isPolynomial{X}(F^N)),
                            (@symbolic isPolynomial{X}(F)),
                            :X,:F,:N=>isConcreteInteger,
                            transform=[:_=>(checktrue,@symbolic N>0)]),

                       # Symbolic Equality
                       Rule((@symbolic _strict{A===B}),
                            (@symbolic R),
                            :A,:B,
                            transform=[:R=>(FunctionWrap((s,c)->Concrete(s.args[1]==s.args[2])),@symbolic A===B)]),
                       
                       # Equality/Inequality

                       Rule((@symbolic _strict{A==B}),
                            (@symbolic R),:A,:B=>(s,c)->s!=(@symbolic 0),
                            transform=[:R=>(checkbool,@symbolic A-B==0)]),

                       Rule((@symbolic _strict{A<=B}),
                            (@symbolic R),:A,:B=>(s,c)->s!=(@symbolic 0),
                            transform=[:R=>(checkbool,@symbolic A-B<=0)]),
                       
                       Rule((@symbolic _strict{A<B}),
                            (@symbolic R),:A,:B=>(s,c)->s!=(@symbolic 0),
                            transform=[:R=>(checkbool,@symbolic A-B<0)]),
                       
                       Rule((@symbolic A>B),
                            (@symbolic R),:A,:B,
                            transform=[:R=>(checkbool,@symbolic B-A<0)]),
                       
                       Rule((@symbolic A>=B),
                            (@symbolic R),:A,:B,
                            transform=[:R=>(checkbool,@symbolic B-A<=0)]),

                       Rule((@symbolic _strict{A<0}),
                            (@symbolic R),
                            :A,
                            transform=[:R=>(FunctionWrap(ConcreteArithmetic.interval_eval_negative),@symbolic A),
                                       :_=>(checkbool,@symbolic R)]),

                       Rule((@symbolic _strict{A<=0}),
                            (@symbolic R),
                            :A,
                            transform=[:R=>(FunctionWrap(ConcreteArithmetic.interval_eval_negative),@symbolic A),
                                       :_=>(checkbool,@symbolic R)]),
                       
                       Rule((@symbolic _strict{X==0}),(@symbolic false),:X,
                            transform=[:_=>(checktrue,@symbolic isGeneric(X))]),

                       Rule((@symbolic _strict{X^N==0}),(@symbolic Y),:X,:N,
                            transform=[:_=>(checktrue,@symbolic (N>0)∨(isInteger(N))),
                                       :Y=>(checkbool,@symbolic X==0)]),

                       Rule((@symbolic _strict{A<0}),(@symbolic R),:A,
                            transform=[:R=>(checkbool,@symbolic _groebnertest{A<0})]),
                       Rule((@symbolic _strict{A<=0}),(@symbolic R),:A,
                            transform=[:R=>(checkbool,@symbolic _groebnertest{A<=0})]),
                       Rule((@symbolic _strict{A==0}),(@symbolic R),:A,
                            transform=[:R=>(checkbool,@symbolic _groebnertest{A==0})]),

# The following are slow and perhaps unneeded...                       
#                       Rule((@symbolic _strict{A==B}),
#                            (@symbolic false),:A,:B,
#                            transform=[:_=>(checktrue,@symbolic (A<B)∨(A>B))]),
#                       Rule((@symbolic _strict{A<=B}),
#                            (@symbolic R),:A,:B,
#                            transform=[:R=>(checkbool,@symbolic (A<B)∨(A==B))]),



                       
                       # Genericity 
                       Rule((@symbolic isGeneric(F)),
                            (@symbolic true),:F,
                            transform=[:A=>(standard,@symbolic contextvalue{assumptions}),
                                       :X=>(Rule((@symbolic _assumptions{commute(isGeneric(X)),R...}),
                                                 (@symbolic X),:X,:R),@symbolic A),
                                       :C=>(checktrue,@symbolic isGeneric(F,X))]),

                       Rule((@symbolic isGeneric(X,X)),
                            (@symbolic true),:X),

                       Rule((@symbolic isGeneric(F^N,X)),
                            (@symbolic isGeneric(F,X)),:F,:N,:X,
                            transform=[:_=>(standard,@symbolic isFree{N,X}∧(¬(N==0)))]),
                       
                       Rule((@symbolic isGeneric(C,X)),
                            (@symbolic false),:C,:X,
                            transform=[:_=>(checktrue,@symbolic isFree{C,X})]),

                       Rule((@symbolic isGeneric(+(commute(A),B...),X)),
                            (@symbolic isGeneric(+(B...),X)),:A,:B,:X,
                            transform=[:_=>(checktrue,@symbolic isFree{A,X})]),

                       Rule((@symbolic isGeneric(*(commute(A),B...),X)),
                            (@symbolic isGeneric(*(B...),X)),:A,:B,:X,
                            transform=[:_1=>(checktrue,@symbolic isFree{A,X}),
                                       :_2=>(checktrue,@symbolic ¬(A==0))]),

                       NoLoop((@symbolic isGeneric(F,X)),
                              (@symbolic isGenericByExpansion(F,X)),
                              (@symbolic isGeneric(F,X)),
                              Rule((@symbolic isGeneric(F,X)), # if F is polynomial in X - expand, and see if any terms are generic
                                   (@symbolic true),:F,:X,
                                   transform=[:_1=>(checktrue,@symbolic ¬isFree{F,X}),
                                              :_2=>(checktrue,@symbolic isPolynomial{X}(F)),
                                              :E=>(standard,@symbolic expand{X}(F)),
                                              :M=>(Reduce(RewriterList([#
                                                                        Rule((@symbolic ∨(G...){+(T1,T2...)}{X}),
                                                                             (@symbolic ∨(G...,isGeneric(T1,X)){+(T2...)}{X}),
                                                                             :G,:T1,:T2,:X),
                                                                        Rule((@symbolic ∨(G...){T1}{X}),
                                                                             (@symbolic ∨(G...,isGeneric(T1,X)){}{X}),
                                                                             :G,:T1,:X),
                                                                        
                                                                        Rule((@symbolic ∨(G...){}{X}),
                                                                             (@symbolic ∨(G...)),:G,:T,:X),
                                                                        
                                                                        ])),@symbolic ∨(){E}{X}),
                                              :_3=>(checktrue,@symbolic M)
                                              ]),
                              :F,:X),

                       # Continuity

                       Rule((@symbolic isContinuous{X}(X)),
                            (@symbolic true),:X=>(s,c)->s isa Atom),

                       Rule((@symbolic isContinuous{X}(C)),
                            (@symbolic true),
                            :X=>(s,c)->s isa Atom,
                            :C,
                            transform=[:_=>(checktrue,@symbolic isFree{C,X}∧isFinite(C))]),

                       Rule((@symbolic isContinuous{X}(+(F1,F2...))),
                            (@symbolic true),
                            :X=>(s,c)->s isa Atom,
                            :F1,:F2,
                            transform=[:_=>(checktrue,@symbolic isContinuous{X}(F1)∧isContinuous{X}(+(F2...)))]),

                       Rule((@symbolic isContinuous{X}(*(F1,F2...))),
                            (@symbolic true),
                            :X=>(s,c)->s isa Atom,
                            :F1,:F2,
                            transform=[:_=>(checktrue,@symbolic isContinuous{X}(F1)∧isContinuous{X}(*(F2...)))]),
                       
                       Rule((@symbolic isContinuous{X}(F^G)),
                            (@symbolic true),
                            :X=>(s,c)->s isa Atom,
                            :F,:G,
                            transform=[:_1=>(checktrue,@symbolic isContinuous{X}(F)∧isContinuous{X}(G)),
                                       :_2=>(checktrue,@symbolic (G>=0)∨¬(F==0))]),
                       
                       ]))


register(defaultregistry,:simplification,
         RewriterList([# _id
                       Rule((@symbolic _strict{_id(X)}),(@symbolic X),:X),

                       SpecialRewriters.LambdaApply(),

                       # Change real complex rational to rational bigint
                       Rule((@symbolic N),
                            (@symbolic R),
                            :N=>(s,c)->(s isa Concrete{Complex{Rational{BigInt}}})&&(s.value.im==0),
                            transform=[:R=>(FunctionWrap((s,c)->Concrete(s.value.re)),@symbolic N)]),
                       
                       # Multiply by zero
                       Rule((@symbolic *(commute(0),commute(X),Y...)),
                            (@symbolic *(0,Y...)),:X,:Y,
                            transform=[:_=>(checktrue,@symbolic isFinite(X))]),

                       Rule((@symbolic *(commute(0),commute(+(X1,X...)),Y...)),
                            (@symbolic *(0*X1+0*(+(X...)),Y...)),:X1,:X,:Y),

                       # Cancel out powers
                       Rule((@symbolic *(X...,Y,Y^N,Z...)),
                            (@symbolic *(X...,Y^(N+1),Z...)),
                            :X,:Y=>(s,c)->!isConcretePositive(s,c),
                            :Z,:N,
                            transform=[:_=>(checktrue,@symbolic ¬(Y==0))]),
                       Rule((@symbolic *(X...,Y^N,Y,Z...)),
                            (@symbolic *(X...,Y^(N+1),Z...)),
                            :X,:Y=>(s,c)->!isConcretePositive(s,c),
                            :Z,:N,
                            transform=[:_=>(checktrue,@symbolic ¬(Y==0))]),
                       Rule((@symbolic _strict{*(X...,Y^M,Y^N,Z...)}),
                            (@symbolic *(X...,Y^(M+N),Z...)),
                            :X,:Y,:Z,:N,:M,
                            transform=[:_=>(checktrue,@symbolic ¬(Y==0))]),

                       # Surd Expressions

                       Rule((@symbolic A^P),
                            (@symbolic numerator(A)^P*denominator(A)^(-P)),
                            :A=>isConcretePositive,
                            :P=>isConcreteRational,
                            transform=[:_=>(checktrue,@symbolic ¬isInteger(A))]),
                       
                       Rule((@symbolic A^P),
                            (@symbolic A*A^(P-1)),
                            :A=>isConcretePositive,
                            :P=>isConcreteRational,
                            transform=[:_=>(checktrue,@symbolic P>1/2)]),

                       Rule((@symbolic A^P),
                            (@symbolic A^(-1)*A^(P+1)),
                            :A=>isConcretePositive,
                            :P=>isConcreteRational,
                            transform=[:_=>(checktrue,@symbolic P<=-1/2)]),

                       ConcreteArithmetic.FactorSurd(),
                       
                       # Expand frozen Polys
                       Rule((@symbolic _strict{*(P1...,+(commute(S1),S2,S3...),P2...)}),
                            (@symbolic *(P1...,S1,P2...)+*(P1...,+(S2,S3...),P2...)),
                            :P1,:P2,:S1,:S2,:S3,
                            transform=[:_=>(checktrue,@symbolic ¬isFree{S1,𝗫})]),
                       Rule((@symbolic _strict{(+(commute(S1),S2,S3...))^N}),
                            (@symbolic (S1*(+(S1,S2,S3...))+(+(S2,S3...)*(+(S1,S2,S3...))))^(N/2)),
                            :S1,:S2,:S3,:N=>isConcreteInteger,
                            transform=[:_=>(checktrue,@symbolic ¬isFree{S1,𝗫}),
                                       :_1=>(checktrue,@symbolic (N>1)∧isInteger(N/2))]),
                       Rule((@symbolic _strict{(+(commute(S1),S2,S3...))^N}),
                            (@symbolic S1*(+(S1,S2,S3...))^(N-1)+(+(S2,S3...))*(+(S1,S2,S3...))^(N-1)),
                            :S1,:S2,:S3,:N=>isConcreteInteger,
                            transform=[:_=>(checktrue,@symbolic ¬isFree{S1,𝗫}),
                                       :_1=>(checktrue,@symbolic N>1)]),
                       
                       # Other simplifications
                       Rule((@symbolic exp(log(X))),
                            (@symbolic X),:X,
                            transform=[:_=>(checktrue,@symbolic ¬(X==0))]),

                       Rule((@symbolic sin(asin(X))),
                            (@symbolic X),:X),
                       
                       Rule((@symbolic cos(asin(X))),
                            (@symbolic (1-X^2)^(1/2)),:X),

                       Rule((@symbolic tan(asin(X))),
                            (@symbolic X/(1-X^2)^(1/2)),:X),
                       
                       Rule((@symbolic cos(acos(X))),
                            (@symbolic X),:X),
                       
                       Rule((@symbolic sin(acos(X))),
                            (@symbolic (1-X^2)^(1/2)),:X),

                       Rule((@symbolic tan(acos(X))),
                            (@symbolic (1-X^2)^(1/2)/X),:X),

                       Rule((@symbolic tan(atan(X))),
                            (@symbolic X),:X),

                       Rule((@symbolic cos(atan(X))),
                            (@symbolic (X^2+1)^(-1/2)),:X),

                       Rule((@symbolic sin(atan(X))),
                            (@symbolic (X^2+1)^(-1/2)*X),:X),

                       Rule((@symbolic sinh(asinh(X))),
                            (@symbolic X),:X),

                       Rule((@symbolic cosh(asinh(X))),
                            (@symbolic (X^2+1)^(1/2)),:X),

                       Rule((@symbolic tanh(asinh(X))),
                            (@symbolic X*(X^2+1)^(-1/2)),:X),

                       Rule((@symbolic sinh(acosh(X))),
                            (@symbolic ((X-1)*(X+1))^(1/2)),:X),

                       Rule((@symbolic cosh(acosh(X))),
                            (@symbolic X),:X),

                       Rule((@symbolic tanh(acosh(X))),
                            (@symbolic ((X-1)*(X+1))^(1/2)/X),:X),

                       Rule((@symbolic sinh(atanh(X))),
                            (@symbolic (X*((1-X)*(1+X))^(-1/2))),:X),
                       
                       Rule((@symbolic cosh(atanh(X))),
                            (@symbolic (((1-X)*(1+X))^(-1/2))),:X),

                       Rule((@symbolic tanh(atanh(X))),
                            (@symbolic X),:X),

                       
                       Rule((@symbolic (X^A)^B),
                            (@symbolic X^(A*B)),:X,:A,:B,
                            transform=[:_=>(checktrue,@symbolic X>=0)]),

                       Rule((@symbolic (*(commute(X),Y...))^P),
                            (@symbolic X^P*(*(Y...))^P),:X,:Y,:P,
                            transform=[:_=>(checktrue,@symbolic X>=0)]),
                       
                       Rule((@symbolic A!=B),
                            (@symbolic ¬(A==B)),:A,:B),
                       
                       
                       
                       # Sub Operations:
                       MustChange(NoLoop((@symbolic ∂{F,X}),
                                         (@symbolic ∂{X}),
                                         (@symbolic assume{∂{F,X},isGeneric(X)}),
                                         SubContext((@symbolic F),
                                                    (@symbolic F),
                                                    standard,
                                                    :F,
                                                    groupadditions=Dict(:standard=>Set([:derivatives]))),
                                         :F,:X)),

                       MustChange(NoLoop((@symbolic ∫{F,X}),
                                         (@symbolic ∫),
                                         (@symbolic ∫{F,X}),
                                         Rule((@symbolic F),
                                              (@symbolic R2),:F,
                                              transform=[:R1=>(SubContext((@symbolic F),
                                                                          (@symbolic F),
                                                                          Integrals.integrals,
                                                                          :F,
                                                                          groupadditions=Dict(:standard=>Set([:integralsdet]))),
                                                               @symbolic F),
                                                         :R2=>(Reduce(RewriterList([Rule((@symbolic _strict{_∫wrap{F,X}}),
                                                                                         (@symbolic F),:F,:X),
                                                                                    Rule((@symbolic _strict{∫(F,X)}),
                                                                                         (@symbolic ∫{F,X}),:F,:X),
                                                                                    Rule((@symbolic _strict{_id(F)}),
                                                                                         (@symbolic F),:F),
                                                                                    ])),@symbolic _id(R1))]),
                                         :F,:X)),

                       MustChange(NoLoop((@symbolic F),
                                         (@symbolic _surdexpression),
                                         (@symbolic F),
                                         SubContext((@symbolic F),
                                                    (@symbolic F),
                                                    SurdExpressions.surdexpressions,
                                                    :F,
                                                    groupadditions=Dict(:standard=>Set([:surdexpressionsdet]))),
                                         :F=>(s,c)->isSurdExpression(s,c)&&(!isConcreteRational(s,c)))),
                       
                       # Definite Integrals

                       Rule((@symbolic _∫definite{F,X,A,B}),
                            (@symbolic IB-IA),
                            :F,:X,:A,:B,
                            transform=[:I=>(standard,@symbolic ∫{F,X}),
                                       :_1=>(checktrue,@symbolic isContinuous{X}(I)),
                                       :IA=>(standard,@symbolic subst{X,A}(I)),
                                       :IB=>(standard,@symbolic subst{X,B}(I))]
                            ),
                       
                       Rule((@symbolic ∫{F,X,A,B}),
                            (@symbolic R),
                            :F,:X,:A,:B,
                            transform=[:_1=>(checktrue,@symbolic A<=B),
                                       :R=>(standard,@symbolic assume{_∫definite{F,X,A,B},X>=A,X<=B}),
                                       :_2=>(checktrue,@symbolic isFree{R,_∫definite})]
                            ),

                       Rule((@symbolic ∫{F,X,A,B}),
                            (@symbolic R),
                            :F,:X,:A,:B,
                            transform=[:_1=>(checktrue,@symbolic A>B),
                                       :R=>(standard,@symbolic assume{_∫definite{F,X,A,B},X>=B,X<=A}),
                                       :_2=>(checktrue,@symbolic isFree{R,_∫definite})]
                            ),

                       Rule((@symbolic ∫{F,X,A,B}),
                            (@symbolic R),
                            :F,:X,:A,:B,
                            transform=[:R=>(standard,@symbolic _∫definite{F,X,A,B}),
                                       :_2=>(checktrue,@symbolic isFree{R,_∫definite})]
                            ),
                       
                       ]))


function lookuptrigvalue(dict)
    function lookup(s,context)
        if s isa Concrete{Rational{BigInt}}
            k=mod(s.value//2,1)
            if haskey(dict,k)
                return dict[k]
            end
        end
    end
end

function lookupasinvalue(s,context)
    for (k,v) in Trigvalues.sin2π
        if (k>=3//4)||(k<=1//4)
            if Callbacks.firstresult(checktrue((@symbolic $(s)-$(v)==0),context))!=nothing
                x=k
                if x>1//2
                    x=x-1//1
                end
                return @symbolic $(Concrete{Rational{BigInt}}(2*x))*π
            end
        end
    end
end

function lookupatanvalue(s,context)
    for (k,v) in Trigvalues.tan2π
        if (k>=3//4)||(k<=1//4)
            if Callbacks.firstresult(checktrue((@symbolic $(s)-$(v)==0),context))!=nothing
                x=k
                if x>1//2
                    x=x-1//1
                end
                return @symbolic $(Concrete{Rational{BigInt}}(2*x))*π
            end
        end
    end
end

function lookupacosvalue(s,context)
    for (k,v) in Trigvalues.cos2π
        if (k<=1//2)
            if Callbacks.firstresult(checktrue((@symbolic $(s)-$(v)==0),context))!=nothing
                x=k
                return @symbolic $(Concrete{Rational{BigInt}}(2*x))*π
            end
        end
    end
end

register(defaultregistry,:trigeval,
         RewriterList([#

                       Rule((@symbolic sin(X*π)),
                            (@symbolic R),:X=>isConcreteRational,
                            transform=[:R=>(FunctionWrap(lookuptrigvalue(Trigvalues.sin2π)),@symbolic X)]),

                       Rule((@symbolic sin(0)),
                            (@symbolic 0)),

                       Rule((@symbolic sin(π)),
                            (@symbolic 0)),

                       Rule((@symbolic cos(X*π)),
                            (@symbolic R),:X=>isConcreteRational,
                            transform=[:R=>(FunctionWrap(lookuptrigvalue(Trigvalues.cos2π)),@symbolic X)]),

                       Rule((@symbolic cos(0)),
                            (@symbolic 1)),

                       Rule((@symbolic cos(π)),
                            (@symbolic -1)),

                       Rule((@symbolic tan(X*π)),
                            (@symbolic R),:X=>isConcreteRational,
                            transform=[:R=>(FunctionWrap(lookuptrigvalue(Trigvalues.tan2π)),@symbolic X)]),

                       Rule((@symbolic tan(0)),
                            (@symbolic 0)),

                       Rule((@symbolic tan(π)),
                            (@symbolic Undefined)),

                       Rule((@symbolic asin(X)),
                            (@symbolic R),:X=>isSurdExpression,
                            transform=[:R=>(FunctionWrap(lookupasinvalue),@symbolic X)]),
                       
                       Rule((@symbolic acos(X)),
                            (@symbolic R),:X=>isSurdExpression,
                            transform=[:R=>(FunctionWrap(lookupacosvalue),@symbolic X)]),
                       
                       Rule((@symbolic atan(X)),
                            (@symbolic R),:X=>isSurdExpression,
                            transform=[:R=>(FunctionWrap(lookupatanvalue),@symbolic X)]),                       
                       
                       ]))


end
