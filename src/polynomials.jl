module Polynomials

import ..@symbolic
import ..@symbolic0
import ..Symbolics: Concrete
import ..Rewriters: Group,Reduce,RewriterList,Rule,MustChange,FunctionWrap
import ..Rewriters
import ..Registries: register
import ..defaultregistry

const checktrue=Rewriters.CheckTrue(Reduce(Group(:standard)))
const checkbool=Rewriters.CheckBool(Reduce(Group(:standard)))
const standard=Reduce(Group(:standard))

register(defaultregistry,:polynomials,
         RewriterList([# Polynomial form simplifications

                       Rule((@symbolic 𝗣{X}(T1...,_(C1,N1),_(C2,N2),T2...)),
                            (@symbolic 𝗣{X}(T1...,_(C2,N2),_(C1,N1),T2...)),
                            :X,:C1,:N1,:C2,:N2,:T1,:T2,
                            transform=[:_1=>(checktrue,@symbolic N2>N1)]),

                       Rule((@symbolic 𝗣{X}(T1...,_(C1,N1),_(C2,N2),T2...)),
                            (@symbolic 𝗣{X}(T1...,_(C1+C2,N2),T2...)),
                            :X,:C1,:N1,:C2,:N2,:T1,:T2,
                            transform=[:_1=>(checktrue,@symbolic N1==N2)]),
                       
                       Rule((@symbolic 𝗣{X}(T1...,_(0,N),T2...)),
                            (@symbolic 𝗣{X}(T1...,T2...)),
                            :X,:T1,:T2,:N),

                       Rule((@symbolic +(commute(𝗣{X}(A...)),commute(𝗣{X}(B...)),R...)),
                            (@symbolic +(𝗣{X}(A...,B...),R...)),
                            :X,:A,:B,:R),

                       Rule((@symbolic *(commute(𝗣{X}(A...)),commute(𝗣{X}()),R...)),
                            (@symbolic *(𝗣{X}(),R...)),
                            :X,:A,:R),

                       Rule((@symbolic *(commute(𝗣{X}(_(C1,N1),A1...)),commute(𝗣{X}(_(C2,N2),A2...)),R...)),
                            (@symbolic *(𝗣{X}(_(C1*C2,N1+N2))+𝗣{X}(_(C1,N1))*𝗣{X}(A2...)+𝗣{X}(A1...)*𝗣{X}(_(C2,N2))+𝗣{X}(A1...)*𝗣{X}(A2...),R...)),
                            :X,:C1,:N1,:A1,:C2,:N2,:A2,:R),

                       Rule((@symbolic 𝗣{X}(A...)^0),
                            (@symbolic 𝗣{X}(_(1,0))),:X,:A),
                       
                       Rule((@symbolic 𝗣{X}(A1,A2...)^N),
                            (@symbolic 𝗣{X}(A1)*𝗣{X}(A1,A2...)^(N-1)+𝗣{X}(A2...)*𝗣{X}(A1,A2...)^(N-1)),
                            :X,:A1,:A2,:N=>(s,c)->s isa Concrete,
                            transform=[:_1=>(checktrue,@symbolic isInteger(N)∧N>0)]),
                       
                       # Convert to and from polynomials

                       Rule((@symbolic toPolynomialForm{X}(C)),
                            (@symbolic 𝗣{X}(_(C,0))),
                            :X,:C,
                            transform=[:_1=>(checktrue,@symbolic isFree{C,X})]),
                            
                       Rule((@symbolic toPolynomialForm{X}(X^N)),
                            (@symbolic 𝗣{X}(_(1,N))),
                            :X,:N=>(s,c)->s isa Concrete,
                            transform=[:_1=>(checktrue,@symbolic isInteger(N)∧N>0)]),
                       
                       Rule((@symbolic toPolynomialForm{X}(X)),
                            (@symbolic 𝗣{X}(_(1,1))),
                            :X),

                       Rule((@symbolic toPolynomialForm{X}(+(A,B...))),
                            (@symbolic toPolynomialForm{X}(A)+toPolynomialForm{X}(+(B...))),
                            :X,:A,:B),

                       Rule((@symbolic toPolynomialForm{X}(*(A,B...))),
                            (@symbolic toPolynomialForm{X}(A)*toPolynomialForm{X}(*(B...))),
                            :X,:A,:B),

                       Rule((@symbolic toPolynomialForm{X}(^(A,N))),
                            (@symbolic toPolynomialForm{X}(A)^N),
                            :X,:A,:N=>(s,c)->s isa Concrete,
                            transform=[:_1=>(checktrue,@symbolic isInteger(N)∧N>=0)]),
                       
                       Rule((@symbolic fromPolynomialForm(𝗣{X}())),
                            (@symbolic 0),:X),

                       Rule((@symbolic fromPolynomialForm(𝗣{X}(_(C,N),R...))),
                            (@symbolic C*X^N+fromPolynomialForm(𝗣{X}(R...))),
                            :X,:C,:N,:R),

                       # Polynomial Division

                       Rule((@symbolic _strict{polynomialDivide(𝗣{X}(P...),𝗣{X}(Q...))}),
                            (@symbolic polynomialDivide(𝗣{X}(P...),𝗣{X}(Q...),𝗣{X}())),
                            :X,:P,:Q),

                       Rule((@symbolic _strict{polynomialDivide(𝗣{X}(_(C1,N1),A...),𝗣{X}(_(C2,N2),B...),Q)}),
                            (@symbolic polynomialDivide(𝗣{X}(A...)+𝗣{X}(B...)*K,𝗣{X}(_(C2,N2),B...),Q+K)),
                            :X,:C1,:N1,:A,:C2,:N2,:B,:Q,
                            transform=[:_1=>(checktrue,@symbolic (N1>=N2)∧(¬(C2==0))),
                                       :K=>(standard,@symbolic 𝗣{X}(_(-C1/C2,N1-N2)))]),

                       Rule((@symbolic _strict{polynomialDivide(𝗣{X}(_(C1,N1),A...),𝗣{X}(_(C2,N2),B...),Q)}),
                            (@symbolic quotient_remainder(Q1,𝗣{X}(_(C1,N1),A...))),
                            :X,:C1,:N1,:A,:C2,:N2,:B,:Q,
                            transform=[:_1=>(checktrue,@symbolic N2>N1),
                                       :Q1=>(standard,@symbolic Q*𝗣{X}(_(-1,0)))]),

                       Rule((@symbolic _strict{polynomialDivide(𝗣{X}(),A,Q)}),
                            (@symbolic quotient_remainder(Q1,𝗣{X}())),
                            :X,:A,:Q,
                            transform=[:Q1=>(standard,@symbolic Q*𝗣{X}(_(-1,0)))]),

                       # Polynomial GCD

                       Rule((@symbolic _strict{polynomialGCD(𝗣{X}(_(C1,N1),A...),𝗣{X}())}),
                            (@symbolic 𝗣{X}(_(C1,N1),A...)*𝗣{X}(_(1/C1,0))),
                            :X,:C1,:N1,:A,
                            transform=[:_1=>(checktrue,@symbolic ¬(C1==0))]),
 
                       Rule((@symbolic _strict{polynomialGCD(𝗣{X}(_(C1,N1),A...),𝗣{X}(_(C2,N2),B...))}),
                            (@symbolic polynomialGCD(𝗣{X}(_(C2,N2),B...),𝗣{X}(_(C1,N1),A...))),
                            :X,:C1,:N1,:A,:C2,:N2,:B,
                            transform=[:_1=>(checktrue,@symbolic N2>N1)]),

                       Rule((@symbolic _strict{polynomialGCD(A,B)}),
                            (@symbolic polynomialGCD(B,R)),
                            :A,:B,
                            transform=[:QR=>(standard,@symbolic polynomialDivide(A,B)),
                                       :R=>(Rule((@symbolic quotient_remainder(Q,R)),
                                                 (@symbolic R),:Q,:R),@symbolic QR)]),


                       # Rational Functions

                       Rule((@symbolic 𝗥{X}(A,B)),
                            (@symbolic 𝗥{X}(A2,B2)),
                            :X,:A,:B,
                            transform=[:GCD=>(standard,@symbolic polynomialGCD(A,B)),
                                       :_1=>(Rule((@symbolic 𝗣{X}(F...)),
                                                  (@symbolic true),:X,:F),@symbolic GCD),
                                       :_2=>(checktrue,@symbolic ¬(GCD===𝗣{X}(_(1,0)))),
                                       :A1=>(standard,@symbolic polynomialDivide(A,GCD)),
                                       :A2=>(Rule((@symbolic quotient_remainder(𝗣{X}(Q...),𝗣{X}())),
                                                  (@symbolic 𝗣{X}(Q...)),:X,:Q),@symbolic A1),
                                       :B1=>(standard,@symbolic polynomialDivide(B,GCD)),
                                       :B2=>(Rule((@symbolic quotient_remainder(𝗣{X}(Q...),𝗣{X}())),
                                                  (@symbolic 𝗣{X}(Q...)),:X,:Q),@symbolic B1)
                                       ]),

                       Rule((@symbolic 𝗥{X}(A,B)^N),
                            (@symbolic 𝗥{X}(A^N,B^N)),
                            :X,:A,:B,:N=>(s,c)->s isa Concrete,
                            transform=[:_1=>(checktrue,@symbolic isInteger(N)∧N>=0)]),

                       Rule((@symbolic 𝗥{X}(A,B)^N),
                            (@symbolic 𝗥{X}(B^(-N),A^(-N))),
                            :X,:A,:B,:N=>(s,c)->s isa Concrete,
                            transform=[:_1=>(checktrue,@symbolic isInteger(N)∧N<0)]),

                       Rule((@symbolic +(commute(𝗥{X}(A1,B)),commute(𝗥{X}(A2,B)),R...)),
                            (@symbolic +(𝗥{X}(A1+A2,B),R...)),
                            :X,:A1,:A2,:B,:R),

                       Rule((@symbolic +(commute(𝗥{X}(A1,B1)),commute(𝗥{X}(A2,B2)),R...)),
                            (@symbolic +(𝗥{X}(A1*B2+A2*B1,B1*B2),R...)),
                            :X,:A1,:A2,:B1,:B2,:R),

                       Rule((@symbolic *(commute(𝗥{X}(A1,B1)),commute(𝗥{X}(A2,B2)),R...)),
                            (@symbolic *(𝗥{X}(A1*A2,B1*B2),R...)),
                            :X,:A1,:A2,:B1,:B2,:R),
                       
                       # Convert to and from rationals

                       Rule((@symbolic toRationalForm{X}(C)),
                            (@symbolic 𝗥{X}(𝗣{X}(_(C,0)),𝗣{X}(_(1,0)))),
                            :X,:C,
                            transform=[:_1=>(checktrue,@symbolic isFree{C,X})]),
                            
                       Rule((@symbolic toRationalForm{X}(X)),
                            (@symbolic 𝗥{X}(𝗣{X}(_(1,1)),𝗣{X}(_(1,0)))),
                            :X,:C),
                       
                       Rule((@symbolic toRationalForm{X}(+(A,B...))),
                            (@symbolic toRationalForm{X}(A)+toRationalForm{X}(+(B...))),
                            :X,:A,:B),

                       Rule((@symbolic toRationalForm{X}(*(A,B...))),
                            (@symbolic toRationalForm{X}(A)*toRationalForm{X}(*(B...))),
                            :X,:A,:B),

                       Rule((@symbolic toRationalForm{X}(A^N)),
                            (@symbolic toRationalForm{X}(A)^N),
                            :X,:A,:N=>(s,c)->(s isa Concrete),
                            transform=[:_1=>(checktrue,@symbolic isInteger(N))]),

                       Rule((@symbolic fromRationalForm(𝗥{X}(A,B))),
                            (@symbolic fromPolynomialForm(A)/fromPolynomialForm(B)),
                            :X,:A,:B),
                       ]))

end
