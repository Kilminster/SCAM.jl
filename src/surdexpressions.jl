module SurdExpressions

import ..Symbolics: Symbolic,Application,Atom,Concrete
import ..Rewriters: RewriterList,Group,Reduce,CheckTrue,Recursive,BestFirst,FunctionWrap,Rule
import ..SymbolicFunctions: isConcreteRational
import ..defaultregistry
import ..Registries: register
import ..@symbolic
import ..@symbolic0

const checktrue=CheckTrue(Reduce(Group(:standard)))
const standard=Reduce(Group(:standard))

function surdexpressionscore(s::Symbolic)
    return 1
end

function surdexpressionscore(s::Application)
    if s.head isa Atom
        if s.head.name==:(^)
            return surdexpressionscore(s.args[1])*2
        end
        if s.head.name==:(*)
            return maximum(surdexpressionscore.(s.args))
        end
        if s.head.name==:(+)
            return sum(surdexpressionscore.(s.args))
        end
    end
    return 1
end

register(defaultregistry,:surdexpressionsdet,
         RewriterList([#
                       ]))

register(defaultregistry,:surdexpressionsnondet,
         RewriterList([#
                       Rule((@symbolic +(X1,X2,X3...)^P),
                            (@symbolic R^(2*P)),
                            :X1,:X2,:X3,:P=>isConcreteRational,
                            transform=[:_1=>(checktrue,@symbolic isInteger(2*P)∧¬isInteger(P)),
                                       :B=>(standard,@symbolic _splitterms2{X1,X2,X3...}),
                                       :XY=>(Rule((@symbolic bag(commute(XY),Z...)),
                                                  (@symbolic XY),:XY,:Z),@symbolic B),
                                       :X=>(Rule((@symbolic _{X...}{Y...}),
                                                 (@symbolic +(X...)),:X,:Y),@symbolic XY),
                                       :Y=>(Rule((@symbolic _{X...}{Y...}),
                                                 (@symbolic +(Y...)),:X,:Y),@symbolic XY),
                                       :R=>(RewriterList([Rule((@symbolic _{X,Y}),
                                                               (@symbolic sqrt(T1/2)+sqrt(T2/2)),
                                                               :X,:Y,
                                                               transform=[:_1a=>(checktrue,@symbolic Y>0),
                                                                          :_1b=>(checktrue,@symbolic X>Y),
                                                                          :T1=>(standard,@symbolic X+sqrt(X^2-Y^2)),
                                                                          :_2=>(checktrue,@symbolic ¬isSum{T1}),
                                                                          :T2=>(standard,@symbolic X-sqrt(X^2-Y^2)),
                                                                          :_3=>(checktrue,@symbolic ¬isSum{T2})
                                                                          ]),
                                                          Rule((@symbolic _{X,Y}),
                                                               (@symbolic sqrt(T1/2)-sqrt(T2/2)),
                                                               :X,:Y,
                                                               transform=[:_1a=>(checktrue,@symbolic -Y>0),
                                                                          :_1b=>(checktrue,@symbolic X>-Y),
                                                                          :T1=>(standard,@symbolic X+sqrt(X^2-Y^2)),
                                                                          :_2=>(checktrue,@symbolic ¬isSum{T1}),
                                                                          :T2=>(standard,@symbolic X-sqrt(X^2-Y^2)),
                                                                          :_3=>(checktrue,@symbolic ¬isSum{T2})
                                                                          ]),
                                                          ]),@symbolic _{X,Y})
                                       ]),

                       Rule((@symbolic *(commute(+(A1...)^P),commute(+(A2...)^P),R...)),
                            (@symbolic *(expand(+(A1...)*+(A2...))^P,R...)),
                            :A1,:A2,:R,:P=>isConcreteRational,
                            transform=[:_=>(checktrue,@symbolic (+(A1...)>0)∧(+(A2...)>0))]),


                       
#                       Rule((@symbolic +(A,B...)^P),
#                            (@symbolic A^P*(+(B...)/A+1)^P),
#                            :A=>isConcreteRational,:P=>isConcreteRational,:B,
#                            transform=[:_=>(checktrue,@symbolic A>0)]),

#                       Rule((@symbolic +(commute(singletonwrap(*,*(commute(A^P),B1...))),commute(singletonwrap(*,*(commute(A^P),B2...))),R...)),
#                            (@symbolic +(*(A^P,+(*(B1...),*(B2...))),R...)),
#                            :A,:P=>isConcreteRational,:B1,:B2,:R),
                       
                       ]))

surdexpressionreturnable(s)=true


const surdexpressions=BestFirst(Reduce(Group(:standard)),
                                Recursive(Group(:surdexpressions)),
                                surdexpressionscore,
                                surdexpressionreturnable,
                                @symbolic surdexpression_effort)


end
