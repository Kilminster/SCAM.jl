module Symbolics

import ..Symbolic

import Base.show
import Base.replace
import Base.hash
import Base.==

function show(io::IO,s::Symbolic)
    write(io,"@symbolic $(asexpr(s))")
    r=[]
    for (k,v) in symboltypes(s)
        if v!=Union{}
            push!(r,"$(k)::$(v)")
        end
    end
    if length(r)>0
        write(io," ")
        write(io,join(r,","))
    end
end

symboltypes(s::Symbolic)=Dict()


struct Atom{T} <: Symbolic
    name::Symbol
end
asexpr(a::Atom)=a.name
symboltypes(a::Atom{T}) where T=Dict(a.name=>T)

hash(a::Atom,h::UInt)=hash(a.name,h)
==(a::Atom,b::Atom)=false
==(a::Atom{T},b::Atom{T}) where T=a.name==b.name

struct Application <: Symbolic
    head::Symbolic
    args::Vector{Symbolic}
end
asexpr(a::Application)=Expr(:call,asexpr(a.head),asexpr.(a.args)...)
symboltypes(a::Application)=merge(symboltypes(a.head),symboltypes.(a.args)...)

hash(a::Application,h::UInt)=hash(a.head,hash(a.args,h))
==(a::Application,b::Application)=(a.head==b.head)&&(a.args==b.args)


struct Curly <: Symbolic
    head::Symbolic
    args::Vector{Symbolic}
end
asexpr(c::Curly)=Expr(:curly,asexpr(c.head),asexpr.(c.args)...)
symboltypes(c::Curly)=merge(symboltypes(c.head),symboltypes.(c.args)...)

hash(c::Curly,h::UInt)=hash(c.head,hash(c.args,h))
==(a::Curly,b::Curly)=(a.head==b.head)&&(a.args==b.args)
    

struct Concrete{T} <: Symbolic
    value::T
end
asexpr(c::Concrete)=c.value

function asexpr(c::Concrete{Rational{BigInt}})
    if c.value.den==1
        asexpr(Concrete(c.value.num))
    else
        asexpr(Application(Atom{Union{}}(:/),[Concrete(c.value.num),Concrete(c.value.den)]))
    end
end

function asexpr(c::Concrete{Complex{Rational{BigInt}}})
    if c.value.im==0
        return asexpr(Concrete(c.value.re))
    end
    ipart=if c.value.im!=1
        Application(Atom{Union{}}(:*),[Concrete(c.value.im),Atom{Union{}}(:im)])
    else
        Atom{Union{}}(:im)
    end
    if c.value.re==0
        return asexpr(ipart)
    end
    return asexpr(Application(Atom{Union{}}(:+),[Concrete(c.value.re),ipart]))
end

hash(c::Concrete,h::UInt)=hash(c.value,h)
==(a::Concrete,b::Concrete)=false
==(a::Concrete{T},b::Concrete{T}) where T=a.value==b.value


function parsetyps(typs)
    if typs==nothing
        return Dict()
    end
    if typs.head==:(::)
        return Dict(Symbol(typs.args[1])=>typs.args[2])
    end
    if typs.head==:(tuple)
        return merge(parsetyps.(typs.args)...)
    end
    error("Can't parse typs $(typs.head).")
end

function symbolic2(ex,d)
    if ex isa Symbol
        if ex==:im
            return Concrete(Complex{Rational{BigInt}}(im))
        end
        if haskey(d,ex)
            return Atom{d[ex]}(ex)
        else
            return Atom{Union{}}(ex)
        end
    end
    if ex isa Expr
        if ex.head==:call
            return Application(symbolic2(ex.args[1],d),
                               [symbolic2(x,d) for x in ex.args[2:end]])
        end
        if ex.head==:curly
            return Curly(symbolic2(ex.args[1],d),
                         [symbolic2(x,d) for x in ex.args[2:end]])
        end
        if ex.head==:...
            return Application(Atom{Union{}}(:...),
                               [symbolic2(x,d) for x in ex.args[1:end]]) 
        end
    end
    if ex isa Bool
        return Concrete(ex)
    end
    if ex isa Integer
        return Concrete(Rational{BigInt}(ex))
    end
    if ex isa Symbolic
        return ex
    end
    error("Can't parse expression $(ex).")
end

function symbolic1(ex,typs=nothing;func=nothing)
    d=parsetyps(typs)
    d1=Expr(:call,:Dict,Expr[Expr(:call,:(=>),Meta.quot(k),v) for (k,v) in d]...)
    ex2=Expr(:quote,ex)
    if func==nothing
        quote
            d=$d1
            SCAM.Symbolics.symbolic2($ex2,d)
        end
    else
        quote
            d=$d1
            ($func)(SCAM.Symbolics.symbolic2($ex2,d))
        end
    end
end


function replace(s::Symbolic,r::Dict{Symbol,T}) where T
    return s
end

function replace(s::Atom,r::Dict{Symbol,T}) where T
    if haskey(r,s.name)
        return r[s.name]
    else
        return s
    end
end

function replace(s::Union{Application,Curly},r::Dict{Symbol,T}) where T
    head=replace(s.head,r)
    args=Symbolic[]
    for a in s.args
        if a isa Application
            if a.head isa Atom
                if a.head.name==:...
                    if haskey(r,a.args[1].name)
                        for x in r[a.args[1].name]
                            push!(args,x)
                        end
                        continue
                    end
                end
            end
        end
        push!(args,replace(a,r))
    end
    return typeof(s)(head,args)
end

function replace(s::Symbolic,r::Pair...)
    d=Dict{Symbol,Any}()
    for (k,v) in r
        d[k]=v
    end
    return replace(s,d)
end

end
