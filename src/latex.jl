module LaTeX

import ..Symbolic
import ..Symbolics: Concrete,Atom,Application,Curly,asexpr

import Base.show

import LaTeXStrings
using Markdown

latex(s::Symbolic)=("",nothing)

function latex(c::Concrete{Rational{BigInt}})
    if c.value.den==1
        "$(c.value.num)",nothing
    else
        "\\frac{$(c.value.num)}{$(c.value.den)}",:/
    end
end

function latex(c::Concrete{Complex{Rational{BigInt}}})
    if c.value.im==0
        return latex(Concrete(c.value.re))
    else
        return "?",nothing
    end
end

function latex(c::Concrete{Bool})
    if c.value
        "\\operatorname{true}",nothing
    else
        "\\operatorname{false}",nothing
    end
end


function latex(s::Atom)
    x=replace(String(s.name),"_"=>"\\_")
    if length(x)==1
        return x,nothing
    else
        return "\\operatorname{$(x)}",nothing
    end
end

function bracketif(l,ss)
    if l[2] in ss
        return "\\left("*l[1]*"\\right)"
    else
        return l[1]
    end
end

function latex(s::Application)
    if s.head isa Atom
        if s.head.name==:/
            if length(s.args)==2
                a=bracketif(latex(s.args[1]),[])
                b=bracketif(latex(s.args[2]),[])
                return ("\\frac{$(a)}{$(b)}",:/)
            end
        end
        if s.head.name==:^
            if length(s.args)==2
                a=bracketif(latex(s.args[1]),[:/,:*,:+,:-,:^,:∂,:∫])
                b=bracketif(latex(s.args[2]),[])
                return ("$(a)^{$(b)}",:^)
            end
        end
        inequalities=Dict((:>)=>">",(:<)=>"<",(:>=)=>"\\geq",(:<=)=>"\\leq",(:(==))=>"=")
        if haskey(inequalities,s.head.name)
            if length(s.args)==2
                a=bracketif(latex(s.args[1]),[])
                b=bracketif(latex(s.args[2]),[])
                o=inequalities[s.head.name]
                return "$(a) $(o) $(b)",:inequality
            end
        end
        if s.head.name==:+
            r=""
            for a in s.args
                l=strip(bracketif(latex(a),[]))
                if startswith(l,"+")||startswith(l,"-")
                    r=r*l
                else
                    r=r*"+"*l
                end
            end
            if startswith(r,"+")
                r=r[2:end]
            end
            return r,:+
        end
        if s.head.name==:∧
            r=[strip(bracketif(latex(a),[:∨,:inequality])) for a in s.args]
            return join(r,'∧'),:∧
        end
        if s.head.name==:∨
            r=[strip(bracketif(latex(a),[:inequality])) for a in s.args]
            return join(r,'∨'),:∨
        end
        
        if s.head.name==:*
            r=""
            for a in s.args
                s=bracketif(latex(a),[:+,:-,:∂,:∫])
                if endswith(r,['0','1','2','3','4','5','6','7','8','9'])
                    if startswith(s,['0','1','2','3','4','5','6','7','8','9'])
                        r=r*"\\times"
                    end
                end
                r=r*s
            end
            return strip(r),:*
        end
        if s.head.name==:-
            if length(s.args)==2
                a=bracketif(latex(s.args[1]),[])
                b=bracketif(latex(s.args[2]),[:+,:-])
                return "$(a)-$(b)",:-
            end
            if length(s.args)==1
                a=bracketif(latex(s.args[1]),[:+,:-])
                return "-$(a)",:-
            end
        end
        if s.head.name==:√
            if length(s.args)==1
                a=bracketif(latex(s.args[1]),[])
                return "\\sqrt{$(a)}",nothing
            end
        end
    end
    
    args=join([latex(a)[1] for a in s.args],",")
    return (latex(s.head)[1]*"\\left("*args*"\\right)",nothing)
end

function latex(s::Curly)
    if s.head isa Atom
        if s.head.name==:∂
            if (length(s.args)==2)&&(s.args[2] isa Atom)
                a=bracketif(latex(s.args[1]),[:+,:-])
                b=bracketif(latex(s.args[2]),[])
                return "\\frac{\\partial}{\\partial $(b)}$a",:∂
            end
        end
        if s.head.name==:∫
            if (length(s.args)==2)&&(s.args[2] isa Atom)
                a=bracketif(latex(s.args[1]),[])
                b=bracketif(latex(s.args[2]),[])
                return "\\int\\!$a\\,\\mathrm{d}$(b)",:∫
            end
            if (length(s.args)==4)&&(s.args[2] isa Atom)
                a=bracketif(latex(s.args[1]),[])
                b=bracketif(latex(s.args[2]),[])
                c=bracketif(latex(s.args[3]),[])
                d=bracketif(latex(s.args[4]),[])
                return "\\int_{$c}^{$d}\\!$a\\,\\mathrm{d}$(b)",:∫
            end
        end
        if s.head.name==:𝗫
            if (length(s.args)==1)
                a=bracketif(latex(s.args[1]),[])
                return "𝗫_{$a}",nothing
            end
        end
    end        

    args=join([latex(a)[1] for a in s.args],",")
    return (latex(s.head)[1]*"\\left\\{"*args*"\\right\\}",nothing)
end


function aslatex(s::Symbolic)
    return LaTeXStrings.latexstring(latex(s)[1])
end

function show(io::IO,mime::MIME"text/html",s::Symbolic)
    l=latex(s)[1]
    buf=IOBuffer()
    show(buf,s)
    r=Markdown.MD(Markdown.LaTeX(l),Markdown.Code(String(take!(buf))))
    show(io,mime,r)
end

    
end
