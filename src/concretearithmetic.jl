module ConcreteArithmetic

using IntervalArithmetic

import Primes
import ..Rewriter
import ..Symbolic
import ..Symbolics: Application,Atom,Concrete
import ..Callbacks: @stopifdone
import ..@symbolic
import ..@symbolic0


struct Pair <: Rewriter
end

function (r::Pair)(s::Symbolic,context)
    function results(cb)
    end
end

function (r::Pair)(s::Application,context)
    function results(cb)
        if s.head isa Atom
            if length(s.args)==2
                if (s.args[1] isa Concrete)&&(s.args[2] isa Concrete)
                    a=s.args[1].value
                    b=s.args[2].value
                    if s.head.name==(:+)
                        @stopifdone cb(Concrete(a+b))
                    end
                    if s.head.name==(:*)
                        @stopifdone cb(Concrete(a*b))
                    end
                    if s.head.name==:(==)
                        @stopifdone cb(Concrete(a==b))
                    end
                    if s.head.name==:(>)
                        @stopifdone cb(Concrete(a>b))
                    end
                    if s.head.name==:(<)
                        @stopifdone cb(Concrete(a<b))
                    end
                    if s.head.name==:(>=)
                        @stopifdone cb(Concrete(a>=b))
                    end
                    if s.head.name==:(<=)
                        @stopifdone cb(Concrete(a<=b))
                    end
                    if s.head.name==(:^)
                        if (a!=0)
                            if b.den==1
                                @stopifdone cb(Concrete(a^Integer(b)))
                            end
                        end
                    end
                end
            end
        end
    end
end


struct FactorSurd <: Rewriter
end

function (r::FactorSurd)(s::Symbolic,context)
    function results(cb)
    end
end

function (r::FactorSurd)(s::Application,context)
    function results(cb)
        if s.head isa Atom
            if (s.head.name==(:^))&&(length(s.args)==2)
                if (s.args[1] isa Concrete{Rational{BigInt}})&&(s.args[2] isa Concrete{Rational{BigInt}})
                    if (s.args[1].value>0)&&(s.args[1].value.den==1)&&(!Primes.isprime(s.args[1].value.num))
                        if s.args[1].value.num<=10^10
                            args=Symbolic[]
                            for (k,v) in Primes.factor(s.args[1].value.num)
                                p=Concrete{Rational{BigInt}}(k)
                                r=Concrete{Rational{BigInt}}(v)
                                push!(args,@symbolic $(p)^($(r)*$(s.args[2])))
                            end
                            @stopifdone cb(Application((@symbolic *),args))
                        end
                    end
                end
            end
        end
    end
end
    

function interval_eval(s::Symbolic,context)
    error("Don't know how to evaluate $s as interval.")
end

function interval_eval(s::Atom{T},context) where T
    if s==@symbolic π
        return @biginterval(π)
    end
    if T <: Real
        return @biginterval(-∞,∞)
    end
    return error("Don't know how to evaluate $s as interval.")
end

function interval_eval(s::Concrete{Rational{BigInt}},context)
    return @biginterval(s.value)
end

function interval_eval(s::Application,context)
    if s.head isa Atom
        if (s.head.name==:+)
            return +([interval_eval(a,context) for a in s.args]...)
        end
        if (s.head.name==:*)
            return *([interval_eval(a,context) for a in s.args]...)
        end
        if (s.head.name==:^)&&(length(s.args)==2)
            return interval_eval(s.args[1],context)^interval_eval(s.args[2],context)
        end
    end
    return error("Don't now how to evaluate $s as interval.")
end

function interval_eval_negative(s,context)
    a=try
        interval_eval(s,context)
    catch
        return @symbolic neither
    end
    if a>0
        return @symbolic false
    end
    if a<0
        return @symbolic true
    end
    return @symbolic neither
end


end
