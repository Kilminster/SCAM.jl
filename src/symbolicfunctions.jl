module SymbolicFunctions

import ..Symbolic
import ..Symbolics: Concrete,Application,Atom

function commutesMultiplicatively(s::Symbolic,context)
    return true  ## Fix this!
end

isConcreteNumber(::Symbolic,context)=false
isConcreteNumber(::Concrete{Rational{BigInt}},context)=true
isConcreteNumber(::Concrete{Complex{Rational{BigInt}}},context)=true

function isConcreteNumber(ss::Vector{T},context) where T <: Symbolic
    for s in ss
        if !isConcreteNumber(s,context)
            return false
        end
    end
    return true
end

isConcreteRational(::Symbolic,context)=false
isConcreteRational(::Concrete{Rational{BigInt}},context)=true

isConcreteNegative(::Symbolic,context)=false
isConcreteNegative(x::Concrete{Rational{BigInt}},context)=x.value<0
isConcreteNegative(x::Concrete{Complex{Rational{BigInt}}},context)=(x.value.im==0)&&(x.value.re<0)

isConcretePositive(::Symbolic,context)=false
isConcretePositive(x::Concrete{Rational{BigInt}},context)=x.value>0
isConcretePositive(x::Concrete{Complex{Rational{BigInt}}},context)=(x.value.im==0)&&(x.value.re>0)

isConcreteInteger(::Symbolic,context)=false
isConcreteInteger(x::Concrete{Rational{BigInt}},context)=x.value.den==1
isConcreteInteger(x::Concrete{Complex{Rational{BigInt}}},context)=(x.value.im==0)&&(x.value.re.den==1)


isSurdExpression(::Symbolic,context)=false
isSurdExpression(::Concrete{Rational{BigInt}},context)=true
isSurdExpression(::Concrete{Complex{Rational{BigInt}}},context)=true

function isSurdExpression(s::Application,context)
    if s.head isa Atom
        if (s.head.name==:^)&&(length(s.args)==2)
            return isSurdExpression(s.args[1],context)&&(s.args[2] isa Concrete{Rational{BigInt}})
        end
        if (s.head.name==:√)&&(length(s.args)==1)
            return isSurdExpression(s.args[1],context)
        end
        if (s.head.name in [:+,:*,:/,:-])
            return all([isSurdExpression(a,context) for a in s.args])
        end
    end
    return false
end

function isSurdExpression(ss::Vector{T},context) where T <: Symbolic
    for s in ss
        if !isSurdExpression(s,context)
            return false
        end
    end
    return true
end
    

end
