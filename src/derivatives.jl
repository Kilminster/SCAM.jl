module Derivatives

import ..@symbolic
import ..@symbolic0
import ..Rewriters: Rule,RewriterList,Reduce,Group
import ..Rewriters
import ..defaultregistry
import ..Registries: register

const standard=Reduce(Group(:standard))
const checktrue=Rewriters.CheckTrue(Reduce(Group(:standard)))


register(defaultregistry,:derivatives,
         RewriterList([#
                       Rule((@symbolic ∂{F,X}),
                            (@symbolic ∂{F1,X}),
                            :F,:X,
                            transform=[:F1=>(standard,@symbolic F)]),

                       Rule((@symbolic ∂{X,X}),
                            (@symbolic 1),:X),

                       Rule((@symbolic ∂{C,X}),
                            (@symbolic 0),:C,:X,
                            transform=[:_=>(checktrue,@symbolic isFree{C,X})]),

                       Rule((@symbolic ∂{+(F,G...),X}),
                            (@symbolic ∂{F,X}+∂{+(G...),X}),
                            :F,:G,:X),

                       Rule((@symbolic ∂{*(F,G...),X}),
                            (@symbolic *(∂{F,X},G...)+F*∂{*(G...),X}),
                            :F,:G,:X),

                       Rule((@symbolic ∂{F^G,X}),
                            (@symbolic F^G*(log(F)*∂{G,X}+G*∂{F,X}/F)),
                            :F,:G,:X),

                       Rule((@symbolic ∂{F(G),X}),
                            (@symbolic (𝔻{F,1}(G)*∂{G,X})),
                            :F,:G,:H,:X),

                       Rule((@symbolic ∂{F(G,H),X}),
                            (@symbolic (𝔻{F,1}(G,H)*∂{G,X}+𝔻{F,2}(G,H)*∂{H,X})),
                            :F,:G,:H,:X),

                       Rule((@symbolic ∂{IF(C,A,B),X}),
                            (@symbolic IF(C,∂{A,X},∂{B,X})),:C,:A,:B,:X),
                       
                       ]))

end
