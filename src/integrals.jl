module Integrals

import ..Rewriter
import ..Symbolic
import ..Rewriters: Reduce,Group,RewriterList,Rule,MustChange,Recursive,BestFirst,SubContext,CheckTrue,NewAtom
import ..defaultregistry
import ..Contexts: newatom
import ..Registries: register
import ..@symbolic
import ..@symbolic0
import ..Symbolics: Concrete,Atom,Application,Curly
import ..Callbacks: @stopifdone

const checktrue=CheckTrue(Reduce(Group(:standard)))
const standard=Reduce(Group(:standard))

struct WrapIntegralComplete <: Rewriter
end

function (r::WrapIntegralComplete)(s,context)
    noIntegral(s,x)=true
    noIntegral(s::Curly,x::Symbolic)=noIntegral(s.head,x)&&all(a->noIntegral(a,x),s.args)

    function noIntegral(s::Application,x::Symbolic)
        if s.head isa Atom
            if s.head.name==:∫
                if s.args[2]==x
                    return false
                end
            end
        end
        return noIntegral(s.head,x)&&all(a->noIntegral(a,x),s.args)
    end
    
    function match(cb)
        if noIntegral(s.args[1],s.args[2])
            @stopifdone cb(@symbolic true)
        end
    end
end


struct Substitutions <: Rewriter
end

function (r::Substitutions)(s::Symbolic,context)

    subexps(s::Symbolic)=Set{Symbolic}()
    function subexps(s::Application)
        r=Set{Symbolic}([s])
        r=union(r,subexps(s.head))
        for a in s.args
            r=union(r,subexps(a))
        end
        return r
    end

    replace(s::Symbolic,sub::Symbolic,u::Atom)=s
    function replace(s::Application,sub::Symbolic,u::Atom)
        if s==sub
            return u
        end
        head1=replace(s.head,sub,u)
        args1=Symbolic[]
        for a in s.args
            push!(args1,replace(a,sub,u))
        end
        return Application(head1,args1)
    end
    
    function results(cb)
        u=newatom(context,(@symbolic _substitutions{s}),Union{})
        for sub in subexps(s)
            r=@symbolic subst{$(u),$(sub)}($(replace(s,sub,u)))
            @stopifdone cb(r)
        end
    end
end


register(defaultregistry,:integralsdet,
         RewriterList([# Wrap integrals:

                       Rule((@symbolic ∫{F,X}),
                            (@symbolic _∫wrap{∫(F,X),X}),
                            :F,:X),

                       # Simplify Wrapped Contents
                       
                       MustChange(Rule((@symbolic _∫wrap{F,X,A...}),
                                       (@symbolic _∫wrap{F1,X,A...}),:F,:X,:A,
                                       transform=[:F1=>(standard,@symbolic assume{F,isGeneric(X),A...})])),

                       # Table of integrals

                       Rule((@symbolic ∫(C,X)),
                            (@symbolic C*X),:C,:X,
                            transform=[:_=>(checktrue,@symbolic isFree{C,X})]),

                       Rule((@symbolic ∫(X,X)),
                            (@symbolic X^2/2),:X),

                       Rule((@symbolic ∫(X^N,X)),
                            (@symbolic IF(N==-1,log(X),X^(N+1)/(N+1))),:X,:N),

                       Rule((@symbolic ∫(cos(X),X)),
                            (@symbolic sin(X)),:X),
                       
                       Rule((@symbolic ∫(sin(X),X)),
                            (@symbolic -cos(X)),:X),

                       Rule((@symbolic ∫(exp(X),X)),
                            (@symbolic exp(X)),:X),

                       Rule((@symbolic ∫(√(X)^(-1),X)),
                            (@symbolic 2*√(X)),:X),
                       
                       Rule((@symbolic ∫(B^X,X)),
                            (@symbolic B^X/log(B)),:B,:X,
                            transform=[:_1=>(checktrue,@symbolic isFree{B,X}),
                                       :_2=>(checktrue,@symbolic B>0)]),

                       Rule((@symbolic ∫(log(X),X)),
                            (@symbolic X*log(X)-X),:X),

                       Rule((@symbolic ∫(cos(X)^N,X)),
                            (@symbolic cos(X)^(N-1)*sin(X)/N+(N-1)/N*∫(cos(X)^(N-2),X)),
                            :X,:N=>(s,c)->s isa Concrete,
                            transform=[:_=>(checktrue,@symbolic isInteger(N)∧(N>1))]),

                       Rule((@symbolic ∫(sin(X)^N,X)),
                            (@symbolic -sin(X)^(N-1)*cos(X)/N+(N-1)/N*∫(sin(X)^(N-2),X)),
                            :X,:N=>(s,c)->s isa Concrete,
                            transform=[:_=>(checktrue,@symbolic isInteger(N)∧(N>1))]),

                       Rule((@symbolic ∫(cos(X)^(-1),X)),
                            (@symbolic log((1+sin(X))/(1-sin(X)))/2),:X),

                       Rule((@symbolic ∫(sin(X)^(-1),X)),
                            (@symbolic -log((1+cos(X))/(1-cos(X)))/2),:X),

                       Rule((@symbolic ∫(cos(X)^N,X)),
                            (@symbolic sin(X)*cos(X)^(N+1)/(-N-1)+(-N-2)/(-N-1)*∫(cos(X)^(N+2),X)),
                            :X,:N=>(s,c)->s isa Concrete,
                            transform=[:_=>(checktrue,@symbolic isInteger(N)∧(N<-1))]),

                       Rule((@symbolic ∫(sin(X)^N,X)),
                            (@symbolic -cos(X)*sin(X)^(N+1)/(-N-1)+(-N-2)/(-N-1)*∫(sin(X)^(N+2),X)),
                            :X,:N=>(s,c)->s isa Concrete,
                            transform=[:_=>(checktrue,@symbolic isInteger(N)∧(N<-1))]),

                       Rule((@symbolic ∫(exp(singletonwrap(*,*(commute(X^2),A...))),X)),
                            (@symbolic (π)^(1/2)*erfi((*(A...))^(1/2)*X)/2/(*(A...))^(1/2)),:X,:A,
                            transform=[:_=>(checktrue,@symbolic *(A...)>0)]),

                       Rule((@symbolic ∫(exp(singletonwrap(*,*(commute(X^2),A...))),X)),
                            (@symbolic (π)^(1/2)*erf((-*(A...))^(1/2)*X)/2/(-*(A...))^(1/2)),:X,:A),                            

                       Rule((@symbolic ∫(cosh(X),X)),
                            (@symbolic sinh(X)),:X),

                       Rule((@symbolic ∫(sinh(X),X)),
                            (@symbolic cosh(X)),:X),

                       Rule((@symbolic ∫(cosh(X)^N,X)),
                            (@symbolic sinh(X)*cosh(X)^(N-1)/N+(N-1)/N*∫(cosh(X)^(N-2),X)),
                            :X,:N=>(s,c)->s isa Concrete,
                            transform=[:_=>(checktrue,@symbolic isInteger(N)∧(N>1))]),

                       Rule((@symbolic ∫(sinh(X)^N,X)),
                            (@symbolic cosh(X)*sinh(X)^(N-1)/N-(N-1)/N*∫(sinh(X)^(N-2),X)),
                            :X,:N=>(s,c)->s isa Concrete,
                            transform=[:_=>(checktrue,@symbolic isInteger(N)∧(N>1))]),

                       Rule((@symbolic ∫(cosh(X)^(-1),X)),
                            (@symbolic 2*atan(exp(X))),:X),

                       Rule((@symbolic ∫(sinh(X)^(-1),X)),
                            (@symbolic log(tanh(X/2))),:X),
                       
                       Rule((@symbolic ∫(cosh(X)^N,X)),
                            (@symbolic -sinh(X)*cosh(X)^(N+1)/(N+1)+(N+2)/(N+1)*∫(cosh(X)^(N+2),X)),
                            :X,:N=>(s,c)->s isa Concrete,
                            transform=[:_=>(checktrue,@symbolic isInteger(N)∧(N<-1))]),

                       Rule((@symbolic ∫(sinh(X)^N,X)),
                            (@symbolic cosh(X)*sinh(X)^(N+1)/(N+1)-(N+2)/(N+1)*∫(sinh(X)^(N+2),X)),
                            :X,:N=>(s,c)->s isa Concrete,
                            transform=[:_=>(checktrue,@symbolic isInteger(N)∧(N<-1))]),

                       # Extract constants:

                       Rule((@symbolic ∫(*(commute(C),F...),X)),
                            (@symbolic C*∫(*(F...),X)),:C,:F,:X,
                            transform=[:_=>(checktrue,@symbolic isFree{C,X})]),

                       # Substitution
                       
                       Rule((@symbolic _∫{sub,1,U,GX,X}(F)),
                            (@symbolic _∫{sub,2,U,GX}(_∫wrap{∫(F,U),U,U==GX})),:U,:GX,:X,:F,
                            transform=[:_=>(checktrue,@symbolic isFree{F,X})]),
                      
                       Rule((@symbolic _∫{sub,2,U,GX}(F)),
                            (@symbolic subst{U,GX}(F)),:U,:GX,:F,
                            transform=[:_=>(checktrue,@symbolic isFree{F,∫}∧isFree{F,_∫})]),

                       # Structural substitution of form ∫{f(x,g(x)),x}
                       
                       Rule((@symbolic _∫{sub,a1,subst{G,GX}(F),U,X}),
                            (@symbolic _∫{sub,a2,F,U,X,G,R}),:G,:GX,:F,:U,:X,
                            transform=[:R=>(standard,@symbolic _subfor{GX,X,U})]),
                       
                       Rule((@symbolic _∫{sub,a2,F,U,X,G,_substitution{GU,XU,UX,DD}}),
                            (@symbolic _∫{sub,1,U,UX,X}(F2*DD)),
                            :F,:U,:X,:G,:GU,:XU,:UX,:DD,
                            transform=[:F1=>(standard,@symbolic subst{G,GU}(F)),
                                       :F2=>(standard,@symbolic subst{X,XU}(F1)),
                                       :_=>(checktrue,@symbolic isFree{F2,Undefined})]),
                       
                                       
                       # Table of structural subsitutions, _subfor{GX,X,U} = > _substitution{GU,XU,UX,DD}
                       # GU - G as function of U
                       # XU - X as function of U or Undefined
                       # UX - U as function of X
                       # DD - dx/du as function of U

                       # exp(x)
                       Rule((@symbolic _subfor{exp(X),X,U}),
                            (@symbolic _substitution{U,Undefined,exp(X),1/U}),:X,:U),

                       # (a^2-b^2x^2)^p
                       Rule((@symbolic _subfor{(+(commute(singletonwrap(*,*(commute(X^2),B...))),A...))^P,X,U}),
                            (@symbolic _substitution{(a*cos(U))^(2*P),a/b*sin(U),asin(b*X/a),a/b*cos(U)}),
                            :X,:U,:A,:B,:P,
                            transform=[:_1=>(checktrue,@symbolic ((¬isInteger(P))∨(P<0))∧isInteger(2*P)),
                                       :_2=>(checktrue,@symbolic isFree{+(A...),X}∧isFree{*(B...),X}),
                                       :_3=>(checktrue,@symbolic (+(A...)>0)∧(*(B...)<0)),
                                       :a=>(standard,@symbolic +(A...)^(1/2)),
                                       :b=>(standard,@symbolic (-(*(B...)))^(1/2))]),

                       # (a^2+b^2x^2)^p
                       Rule((@symbolic _subfor{(+(commute(singletonwrap(*,*(commute(X^2),B...))),A...))^P,X,U}),
                            (@symbolic _substitution{(a/cos(U))^(2*P),a/b*tan(U),atan(b*X/a),a/b*cos(U)^(-2)}),
                            :X,:U,:A,:B,:P,
                            transform=[:_1=>(checktrue,@symbolic (P<0)∧isInteger(P)),
                                       :_2=>(checktrue,@symbolic isFree{+(A...),X}∧isFree{*(B...),X}),
                                       :_3=>(checktrue,@symbolic (+(A...)>0)∧(*(B...)>0)),
                                       :a=>(standard,@symbolic +(A...)^(1/2)),
                                       :b=>(standard,@symbolic (*(B...))^(1/2))]),
                       
                       Rule((@symbolic _subfor{(+(commute(singletonwrap(*,*(commute(X^2),B...))),A...))^P,X,U}),
                            (@symbolic _substitution{(a*cosh(U))^(2*P),a/b*sinh(U),asinh(b*X/a),a*cosh(U)/b}),
                            :X,:U,:A,:B,:P,
                            transform=[:_1=>(checktrue,@symbolic ((¬(isInteger(P)))∧isInteger(2*P))),
                                       :_2=>(checktrue,@symbolic isFree{+(A...),X}∧isFree{*(B...),X}),
                                       :_3=>(checktrue,@symbolic (+(A...)>0)∧(*(B...)>0)),
                                       :a=>(standard,@symbolic +(A...)^(1/2)),
                                       :b=>(standard,@symbolic (*(B...))^(1/2))]),
                            
                       # (b^2x^2-a^2)^p
                       Rule((@symbolic _subfor{(+(commute(singletonwrap(*,*(commute(X^2),B...))),A...))^P,X,U}),
                            (@symbolic _substitution{(a*sinh(U))^(2*P),a/b*cosh(U),acosh(b*X/a),a/b*sinh(U)}),
                            :X,:U,:A,:B,:P,
                            transform=[:_1=>(checktrue,@symbolic ((¬(isInteger(P)))∧isInteger(2*P))),
                                       :_2=>(checktrue,@symbolic isFree{+(A...),X}∧isFree{*(B...),X}),
                                       :_3=>(checktrue,@symbolic (+(A...)<0)∧(*(B...)>0)),
                                       :a=>(standard,@symbolic (-(+(A...)))^(1/2)),
                                       :b=>(standard,@symbolic (*(B...))^(1/2))]),

                       Rule((@symbolic _subfor{(+(commute(singletonwrap(*,*(commute(X^2),B...))),A...))^P,X,U}),
                            (@symbolic _substitution{(-1)^P*(a*cos(U))^(2*P),a/b*sin(U),asin(b*X/a),a/b*cos(U)}),
                            :X,:U,:A,:B,:P,
                            transform=[:_1=>(checktrue,@symbolic (P<0)∧isInteger(P)),
                                       :_2=>(checktrue,@symbolic isFree{+(A...),X}∧isFree{*(B...),X}),
                                       :_3=>(checktrue,@symbolic (+(A...)<0)∧(*(B...)>0)),
                                       :a=>(standard,@symbolic (-(+(A...)))^(1/2)),
                                       :b=>(standard,@symbolic (*(B...))^(1/2))]),

                       # ax^2+bx+c - Completing the square
                       Rule((@symbolic _subfor{+(commute(singletonwrap(*,*(commute(X^2),A...))),commute(singletonwrap(*,*(commute(X),B...))),C...),X,U}),
                            (@symbolic _substitution{a*U^2+c-b^2/4/a,U-b/2/a,X+b/2/a,1}),
                            :X,:U,:A,:B,:C,
                            transform=[:a=>(standard,@symbolic *(A...)),
                                       :_1=>(checktrue,@symbolic isFree{a,X}),
                                       :b=>(standard,@symbolic *(B...)),
                                       :_2=>(checktrue,@symbolic isFree{b,X}),
                                       :c=>(standard,@symbolic +(C...)),
                                       :_3=>(checktrue,@symbolic isFree{c,X}),
                                       :_4=>(checktrue,@symbolic ¬(a==0))]),

                       # sqrt(a+bx) # commented out for now, but uncomment after partial fractions is done
#                       Rule((@symbolic _subfor{(+(commute(singletonwrap(*,*(commute(X),B...))),A...))^P,X,U}),
#                            (@symbolic _substitution{U^(2*P),(U^2-a)/b,(a+b*X)^(1/2),b/2/U}),
#                            :X,:U,:A,:B,:P,
#                            transform=[:_1=>(checktrue,@symbolic (¬(isInteger(P)))∧isInteger(2*P)),
#                                       :_2=>(checktrue,@symbolic isFree{+(A...),X}∧isFree{*(B...),X}),
#                                       :_3=>(checktrue,@symbolic ¬(*(B...)==0)),
#                                       :a=>(standard,@symbolic +(A...)),
#                                       :b=>(standard,@symbolic *(B...))]),
                       
                       
                       
                       # Parts

                       Rule((@symbolic _∫{parts1,X,*()}(A,B)),
                            (@symbolic _∫{parts2,X}(_e(1,A,B))),
                            :X,:A,:B,
                            transform=[:_=>(checktrue,@symbolic (¬isFree{A,X})∧(¬isFree{B,X}))]),

                       Rule((@symbolic _∫{parts2,X}(_e(N1,A1,B1),E...,_e(N2,A2,B2))),
                            (@symbolic _∫{parts3,A1*B1,X}(1-M,0,_e(N1,A1,B1),E...,_e(N2,A2,B2))),
                            :X,:N1,:A1,:B1,:N2,:A2,:B2,:E,
                            transform=[:M=>(standard,@symbolic (N2*A2*B2)/(N1*A1*B1)),
                                       :_=>(checktrue,@symbolic isFree{M,∫}∧isFree{M,_∫}∧isFree{M,X}∧isFree{M,Undefined})]),

                       Rule((@symbolic _∫{parts3,F,X}(M,S,_e(N1,A1,B1),_e(N2,A2,B2),E...)),
                            (@symbolic _∫{parts3,F,X}(M,S+N1*A1*B2,_e(N2,A2,B2),E...)),
                            :F,:X,:M,:S,:N1,:A1,:B1,:N2,:A2,:B2,:E),
                       
                       Rule((@symbolic _∫{parts3,F,X}(M,S,E)),
                            (@symbolic S/M),
                            :F,:X,:M,:S,:E,
                            transform=[:_=>(checktrue,@symbolic ¬(M==0))]),
                       
                       
                       # Unwrap complete integrals:

                       Rule((@symbolic _∫wrap{F,X,A...}),
                            (@symbolic F),:F,:X,:A,
                            transform=[:_1=>(checktrue,@symbolic isFree{F,_∫}),
                                       :_2=>(WrapIntegralComplete(),
                                             @symbolic _∫wrap{F,X})]),
                       
                       ]))

register(defaultregistry,:integralsnondet,
         RewriterList([# Wrapped:

                       Rule((@symbolic _∫wrap{F,X,A...}),
                            (@symbolic _∫wrap{R2,X,A...}),:F,:X,:A,
                            transform=[:R1=>(Reduce(Group(:assumptions)),@symbolic assume{F,isGeneric(X),A...}),
                                       :R2=>(SubContext((@symbolic context{F,X...}),
                                                        (@symbolic F),
                                                        Recursive(Group(:integrals)),
                                                        :F,:X,
                                                        newvalues=(@symbolic _newvalues{X...})),
                                             @symbolic R1)]),

                       # Sums:

                       Rule((@symbolic ∫(+(commute(F1),F2...),X)),  # decompose sum one at a time
                            (@symbolic ∫(F1,X)+∫(+(F2...),X)),:F1,:F2,:X),

                       Reduce(Rule((@symbolic ∫(+(commute(F1),F2...),X)),  # or all at once
                                   (@symbolic ∫(F1,X)+∫(+(F2...),X)),:F1,:F2,:X)),

                       Rule((@symbolic ∫(*(commute(+(commute(F1),F2...)),R...),X)), # or from a product
                            (@symbolic ∫(*(F1,R...),X)+∫(*(+(F2...),R...),X)),:F1,:F2,:R,:X),
                       
                       # Expand wrt X:

                       Rule((@symbolic ∫(F,X)),
                            (@symbolic ∫(expand{X}(F),X)),:F,:X),

                       # Equivalents:

                       Rule((@symbolic √(X)),
                            (@symbolic X^(1/2)),:X),

                       # Substitution:

                       Rule((@symbolic ∫(F,X)),
                            (@symbolic R),:F,:X,
                            transform=[:S=>(Substitutions(),@symbolic F),
                                       :G=>(Rule((@symbolic subst{U,G}(H)),
                                                 (@symbolic G),:U,:G,:H),@symbolic S),
                                       :D=>(standard,@symbolic ∂{G,X}),
                                       :_=>(checktrue,@symbolic ¬(D==0)),
                                       :R=>(Rule((@symbolic subst{U,G}(H)(F,X,D)),
                                                 (@symbolic _∫{sub,1,U,G,X}(H/D)),:U,:G,:H,:F,:X,:D),
                                            @symbolic S(F,X,D)),
                                       ]),

                       Rule((@symbolic ∫(F,X)),
                            (@symbolic _∫{sub,a1,S,U,X}),:F,:X,
                            transform=[:S=>(Substitutions(),@symbolic F),
                                       :U=>(NewAtom(),@symbolic _intbysubstitution{F,X})
                                       ]),

                       # Parts:

                       Rule((@symbolic ∫(*(F...),X)),
                            (@symbolic _∫{parts1,X,*(F...)}(1,1)),
                            :F,:X),
                       Rule((@symbolic _∫{parts1,X,*(F1,F...)}(A,B)),
                            (@symbolic _∫{parts1,X,*(F...)}(F1*A,B)),
                            :X,:F1,:F,:A,:B),
                       Rule((@symbolic _∫{parts1,X,*(F1,F...)}(A,B)),
                            (@symbolic _∫{parts1,X,*(F...)}(A,F1*B)),
                            :X,:F1,:F,:A,:B),

                       Rule((@symbolic _∫{parts2,X}(T...,_e(N,A,B))),
                            (@symbolic _∫{parts2,X}(T...,_e(N,A,B),_e(-N,∂{A,X},∫{B,X}))),
                            :X,:T,:N,:A,:B,
                            transform=[:_=>(checktrue,@symbolic isFree{A,∂}∧isFree{B,∫}∧isFree{B,_∫})]),
                          
                       ]))


function integralscore(s)
    counts(s::Concrete)=[1,0]
    counts(s::Atom)=[1,s.name in [:∫,:_∫]]
    function counts(s)
        r=counts(s.head)
        for a in s.args
            r=r+counts(a)
        end
        return r
    end        
    integralin(s::Concrete)=false
    integralin(s::Atom)=s.name in [:∫,:_∫]
    integralin(s::Union{Application,Curly})=integralin(s.head)||any(integralin,s.args)
    score(s::Union{Concrete,Atom})=0
    function score(s::Union{Application,Curly})
        if integralin(s.head)
            c=counts(s)
            return c[1]*c[2]
        end
        r=0
        r=r+score(s.head)
        for a in s.args
            r=r+score(a)
        end
        return r
    end
    return score(s)
end

integralreturnable(s)=true
integralreturnable(s::Atom)=!(s.name in [:_∫,:Undefined])
integralreturnable(s::Union{Application,Curly})=integralreturnable(s.head)&&(all(integralreturnable,s.args))



const integrals=BestFirst(Reduce(Group(:standard)),
                          Recursive(Group(:integrals)),
                          integralscore,
                          integralreturnable,
                          @symbolic integration_effort)


end
