module SCAM

export @symbolic,@symbolic0,@s,@sc

include("profile.jl")

include("types.jl")
include("callbacks.jl")
include("symbolics.jl")

import .Symbolics: asexpr

include("latex.jl")

macro symbolic0(ex,typs=nothing,func=x->x)
    Symbolics.symbolic1(ex,typs,func=func)
end

function symbolichelper(mod,s,typs,func)
    ex=quote
        @symbolic0 $s $typs $func
    end
    Expr(:escape, Expr(:call, GlobalRef(Core, :eval), mod, Expr(:quote, ex)))
end

macro symbolic(ex,typs=nothing)
    symbolichelper(__module__,ex,typs,x->x)
end

include("registries.jl")
include("contexts.jl")
include("patterns.jl")
include("rewriters.jl")
include("symbolicfunctions.jl")
include("canonicalorder.jl")
include("concretearithmetic.jl")
include("specialrewriters.jl")

const defaultregistry=Registries.Registry(Dict(:standard=>Set([:basic,:canonical,:basicfunctions,:predicates,
                                                               :simplification,
                                                               :contexts,:functionfacts,:constructassumptions,
                                                               :manipulation,:groebner,:polynomials,:trigeval
                                                               ]),
                                               :pretty=>Set([:basic,:pretty]),
                                               :integrals=>Set([:integralsnondet]),
                                               :assumptions=>Set([:constructassumptions]),
                                               :expand3a=>Set([:expand3a]),
                                               :surdexpressions=>Set([:surdexpressionsnondet]),
                                               ),
                                          Dict((@symbolic assumptions)=>(@symbolic _assumptions{}),
                                               (@symbolic integration_effort)=>(@symbolic 50),
                                               (@symbolic surdexpression_effort)=>(@symbolic 3))
                                          )


include("groebner.jl")
include("integrals.jl")
include("surdexpressions.jl")
include("trigvalues.jl")
include("rules.jl")
include("manipulation.jl")
include("derivatives.jl")
include("functionfacts.jl")
include("groebnerrules.jl")
include("polynomials.jl")

function reduce(s::Symbolic,group::Symbol)
    context=Contexts.Context(defaultregistry)
    r=Rewriters.Reduce(Rewriters.Group(group))
    s1=Callbacks.firstresult(r(s,context))
    if s1==nothing
        return s
    end
    return s1
end

simplify(s)=reduce(reduce(s,:standard),:pretty)

macro s(ex,typs=nothing)
    symbolichelper(__module__,ex,typs,simplify)
end

macro sc(ex,typs=nothing)
    symbolichelper(__module__,ex,typs,s->reduce(s,:standard))
end


end
