using SCAM
using Memoize
import SCAM.Symbolics: Application,Symbolic

@memoize function cos2π(x::Rational)
    x=mod(x,1)
    if x==1//3
        return @s -1/2
    end
    if x==1//5
        return @s (sqrt(5)-1)/4
    end
    if x==1//15
        return @s (1+sqrt(5)+sqrt(6*(5-sqrt(5))))/8
    end
    if x>1/2
        return cos2π(-x)
    end
    if x>1/4
        return @s expand(-$(cos2π(1//2-x)))
    end
    if x==0
        return @symbolic 1
    end
    if x==1/4
        return @symbolic 0
    end
    if mod(x.den,2)==0
        return @s expand(sqrt((2+2*$(cos2π(x*2))))/2)
    end
    if x.num>2
        a=div(x.num,2)//x.den
        b=x-a
        ca=cos2π(a)
        cb=cos2π(b)
        sa=sin2π(a)
        sb=sin2π(b)
        return @s expand($(ca)*$(cb)-$(sa)*$(sb))
    end
    error("Don't know cos2π($x)")
end

@memoize function sin2π(x::Rational)
    x=mod(x,1)
    if x==1//3
        return @s sqrt(3)/2
    end
    if x==1//5
        return @s sqrt(5+sqrt(5))/sqrt(8)
    end
    if x==1//15
        return @s sqrt(7+sqrt(5)-sqrt(6*(5+sqrt(5))))/4
    end
    if x>1/2
        return @s expand(-$(sin2π(-x)))
    end
    if x>1/4
        return sin2π(1//2-x)
    end
    if x==0
        return @symbolic 0
    end
    if x==1/4
        return @symbolic 1
    end
    if mod(x.den,2)==0
        return @s expand(sqrt((2-2*$(cos2π(x*2))))/2)
    end
    if x.num>2
        a=div(x.num,2)//x.den
        b=x-a
        ca=cos2π(a)
        cb=cos2π(b)
        sa=sin2π(a)
        sb=sin2π(b)
        return @s expand($(sa)*$(cb)+$(ca)*$(sb))        
    end
    error("Don't know sin2π($x)")
end

@memoize function tan2π(x::Rational)
    return @s expand($(sin2π(x))/$(cos2π(x)))
end

function test()
    for i=0:720
        x=i//720
        try
            c=cos2π(x)
            s=sin2π(x)
            println(i/2)
            cn=eval(SCAM.asexpr(c))
            sn=eval(SCAM.asexpr(s))
            println(c)
            println(s)
            println(abs(cn-cos(2*pi*x))+abs(sn-sin(2*pi*x))<0.0001)
            println()
        catch err
            #println(err)
        end
    end
end

nesting(s::Symbolic)=0
nesting(s::Application)=max([nesting(a) for a in s.args]...)+(s.head.name==:(√))

function table(maxden,fn,maxnest=2)
    r=Dict{Rational,Symbolic}()
    for i=0:maxden-1
        println(i)
        x=i//maxden
        try
            y=fn(x)
            if nesting(y)<=maxnest
                r[x]=y
            end
        catch
        end
    end
    return r
end

function writetable(fn)
    r=[]
    for (k,v) in table(3*5*32,fn)
        push!(r,"$(k) => ($(v))")
    end
    return "const $(fn) = Dict($(join(r,',')))"
end

function generatecode()
    """module Trigvalues

import ..@symbolic
import ..@symbolic0

$(writetable(sin2π))

$(writetable(cos2π))

$(writetable(tan2π))

end
"""
end

function main()
    f=open("../src/trigvalues.jl","w")
    write(f,generatecode())
    close(f)
end

main()
#test()
