using Test
using SCAM

@testset "Symbolic" begin
    
    @testset "Equality" begin
        @test (@symbolic x)==(@symbolic x)
        @test (@symbolic x)!=(@symbolic y)
        @test (@symbolic x+y)==(@symbolic x+y)
        @test (@symbolic x+y)!=(@symbolic y+x)
        @test (@symbolic x x::Real)!=(@symbolic x)
        @test (@symbolic x+y x::Real,y::Complex)==(@symbolic x+y y::Complex,x::Real)
        @test (@symbolic x+y x::Real,y::Complex)!=(@symbolic x+y)
        @test (@symbolic (1+x)+(1+x))==(@symbolic (1+x)+(1+x))
    end

    @testset "Replacements" begin
        @test replace((@symbolic X+X),:X=>(@symbolic 1+x))==@symbolic (1+x)+(1+x)
        @test replace((@symbolic 1+(x*X)),:X=>(@symbolic 1+x))==@symbolic 1+(x*(1+x))
        @test replace((@symbolic 1+(Y*X)),:X=>(@symbolic 1+x),:Y=>(@symbolic a))==@symbolic 1+(a*(1+x))
        @test replace((@symbolic 1+(Y*X)),Dict(:X=>(@symbolic 1+x),:Y=>(@symbolic a)))==@symbolic 1+(a*(1+x))
        @test replace((@symbolic f(X,Y...,z)),:X=>(@symbolic a),:Y=>[(@symbolic b),(@symbolic c),(@symbolic d)])==@symbolic f(a,b,c,d,z)
    end

end



