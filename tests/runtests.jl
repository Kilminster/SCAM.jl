using Test

@testset "All tests" begin
    include("symbolic.jl")
    include("patterns.jl")
    include("rewriters.jl")
    include("canonical.jl")
    include("integrals.jl")
end
