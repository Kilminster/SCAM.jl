using Test
using SCAM

import SCAM.Callbacks: allresults,firstresult
import SCAM.Rewriters.Rule


@testset "Rewriters" begin

    @testset "TransformRules" begin
    
        rf=Rule((@symbolic X),(@symbolic f(X)),:X)
        rg=Rule((@symbolic a+a+c),(@symbolic g(a+a+c)))
        
        r1=Rule((@symbolic +(commute(X),commute(X),Y...)),
                (@symbolic +(2*X,Y...)),
                :X,:Y
                )
        
        r2=Rule((@symbolic +(commute(X),commute(X),Y...)),
                (@symbolic +(2*X1,Y...)),
                :X,:Y,
                transform=[:X1=>(rf,@symbolic X)]
                )
        
        r3=Rule((@symbolic +(commute(X),commute(X),Y...)),
                (@symbolic +(2*X1,Y1)),
                :X,:Y,
                transform=[:X1=>(rf,@symbolic X),
                           :Y1=>(rg,@symbolic +(Y...))]
                )
        
        
        s=@symbolic a+a+b+c+b
        
        @test firstresult(r1(s,nothing))==@symbolic 2*a+b+c+b
        @test firstresult(r2(s,nothing))==@symbolic 2*f(a)+b+c+b
        @test firstresult(r3(s,nothing))==@symbolic 2*f(b)+g(a+a+c)
    end

    @testset "RewriterList" begin
        r1=Rule((@symbolic +(commute(a),X...)),
                (@symbolic +(A,X...)),:X)
        r2=Rule((@symbolic +(commute(b),X...)),
                (@symbolic +(B,X...)),:X)
        r=SCAM.Rewriters.RewriterList([r1,r2])
        s=@symbolic a+b+c+d+a

        @test length(allresults(r(s,nothing)))==3
    end

    @testset "Reduce" begin
        r1=Rule((@symbolic a),(@symbolic b))
        r2=Rule((@symbolic c),(@symbolic d))
        r3=Rule((@symbolic b),(@symbolic c))
        r4=Rule((@symbolic d{X,Y}),(@symbolic h(X+Y)),:X,:Y)
        r=SCAM.Rewriters.Reduce(SCAM.Rewriters.RewriterList([r1,r2,r3,r4]))
        s=@symbolic a
        @test firstresult(r(s,SCAM.Contexts.Context()))==@symbolic d
        s=@symbolic d
        @test firstresult(r(s,SCAM.Contexts.Context()))==nothing
        s=@symbolic a+d+f(a)+b(c)
        @test firstresult(r(s,SCAM.Contexts.Context()))==@symbolic d+d+f(d)+d(d)
        s=@symbolic a+d+f{a}+b{c}
        @test firstresult(r(s,SCAM.Contexts.Context()))==@symbolic d+d+f{a}+d{c}
        s=@symbolic a+d+f{a}+b{c,f}
        @test firstresult(r(s,SCAM.Contexts.Context()))==@symbolic d+d+f{a}+h(d+f)
    end

    
end
