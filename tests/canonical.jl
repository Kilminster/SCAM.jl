using Test
using SCAM

@testset "Canonical" begin

    @testset "Order" begin
        TESTTERMS=[(@symbolic (3*x*y)^2),
                   (@symbolic x^2*y^2),
                   (@symbolic cos(x+y)),
                   (@symbolic 9*cos(x+y)),
                   (@symbolic (1 + 𝗫{a}) * 𝗫{a} * 𝗫{b}),
                   (@symbolic 𝗫{a} * (1 + 𝗫{a}) * 𝗫{b})]
        
        @testset "Total" begin
            for a in TESTTERMS
                for b in TESTTERMS
                    @test (a==b)||(SCAM.CanonicalOrder.canonical_order(a,b)+
                                   SCAM.CanonicalOrder.canonical_order(b,a)==1)
                end
            end
        end
        
        @testset "Transitive" begin
            for a in TESTTERMS
                for b in TESTTERMS
                    for c in TESTTERMS
                        @test (!(SCAM.CanonicalOrder.canonical_order(a,b)&&
                                 SCAM.CanonicalOrder.canonical_order(b,c))||
                               SCAM.CanonicalOrder.canonical_order(a,c))

                        @test (!(SCAM.CanonicalOrder.canonical_order(b,a)&&
                                 SCAM.CanonicalOrder.canonical_order(c,b))||
                               SCAM.CanonicalOrder.canonical_order(c,a))
                    end
                end
            end
        end
        
    end
end
