using Test
using SCAM
import FunctionWrappers: FunctionWrapper

@testset "Integrals" begin

    INTEGRANDS=[(@symbolic 2/3),
                (@symbolic 3*x),
                (@symbolic x^2+x+2/x),
                (@symbolic x^2*cos(x)),
                ((@symbolic sin(x)^6*cos(sin(x))*cos(x)),200),
                (@symbolic (2*x+1)/2*(x^2+x)),
                (@symbolic exp(x/5)),
                (@symbolic 2^(x/5)),
                (@symbolic x*log(x^2)),
                (@symbolic x*(1+x)^2),
                (@symbolic cos(x)*exp(x)),
                (@symbolic x*cos(1+x^2)/sin(1+x^2)),
                (@symbolic x/2/sqrt(x^2+1)),
                (@symbolic sqrt(3*x)),
                (@symbolic 3^x*log(3)),
                (@symbolic 1/sqrt(3-x^2)),
                (@symbolic exp(x)*(1+exp(x)^2+exp(x)^3)),
                (@symbolic exp(x)*cos(exp(x))),
                (@symbolic exp(x)^2),
                (@symbolic 1/sqrt(9-4*x^2)),
                (@symbolic 1/sqrt(4*x^2-9)),
                (@symbolic 1/cos(x)),
                (@symbolic 1/cos(x)^2),
                (@symbolic 1/cos(x)^5),
                (@symbolic 1/sin(x)),
                (@symbolic 1/sin(x)^2),
                (@symbolic 1/sin(x)^3),
                (@symbolic 1/(5+3*x^2)),
                (@symbolic sqrt(2+x^2)),
                (@symbolic 1/sqrt(1+x^2)),
                ]

    function makefunc(s)
        g=@eval f(x)=$(SCAM.asexpr(s))
        return g |> FunctionWrapper{Float64,Tuple{Float64}}
    end
    
    function probablysame(a,b)
        if (@s $(a)==$(b))==(@symbolic true)
            return true
        end
        fa=makefunc(a) 
        fb=makefunc(b)
        cnt=0
        for i=1:100
            x=randn()*5
            y1=try
                fa(x)
            catch
                nothing
            end
            if y1!=nothing
                y2=fb(x)
                if abs(y1-y2)/max(abs(y1),abs(y2),1.0)>0.01
                    return false
                end
                cnt=cnt+1
            end
        end
        println("Tested at $(cnt) points.")
        return true
    end
    
    for problem in INTEGRANDS
        integrand=if problem isa Tuple
            problem[1]
        else
            problem
        end
        println("f(x): $(integrand)")
        integral=if problem isa Tuple
            effort=@symbolic $(problem[2])
            @s context{∫{$(integrand),x},integration_effort,$(effort)}
        else
            @s ∫{$(integrand),x}
        end
        println("F(x)=∫{f(x),x}: $(integral)")
        @test (@s isFree{$(integral),∫})==@symbolic true
        derivative=@s ∂{$integral,x}
        println("F'(x): $(derivative)")
        @test probablysame(integrand,derivative)
        println()
    end
    
end
