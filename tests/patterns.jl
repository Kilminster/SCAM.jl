using Test
using SCAM

import SCAM.Callbacks: allresults,firstresult

@testset "Patterns" begin
    
    @testset "LiteralAtom" begin
        x_pat=SCAM.Patterns.makepattern(@symbolic x)
        xR_pat=SCAM.Patterns.makepattern(@symbolic x x::Real)
        bindings=Dict{Symbol,Any}()
        context=nothing
        @test length(allresults(x_pat((@symbolic x),bindings,context)))==1
        @test length(allresults(x_pat((@symbolic x x::Real),bindings,context)))==1
        @test length(allresults(xR_pat((@symbolic x),bindings,context)))==1
        @test length(allresults(xR_pat((@symbolic x x::Real),bindings,context)))==1
        @test length(allresults(xR_pat((@symbolic x x::Complex),bindings,context)))==0
        @test length(allresults(x_pat((@symbolic y),bindings,context)))==0
        @test length(allresults(x_pat((@symbolic x+y),bindings,context)))==0
    end

    @testset "Variable" begin
        X=SCAM.Patterns.makepattern((@symbolic X),:X)
        Xatom=SCAM.Patterns.makepattern((@symbolic X),:X=>(s,_)->s isa SCAM.Symbolics.Atom)
        bindings=Dict{Symbol,Any}()
        context=nothing

        @test length(allresults(X((@symbolic x),bindings,context)))==1
        @test length(allresults(X((@symbolic y),bindings,context)))==1
        @test length(allresults(X((@symbolic x+y),bindings,context)))==1

        B=firstresult(X((@symbolic x),bindings,context))
        @test length(allresults(X((@symbolic x),B,context)))==1
        @test length(allresults(X((@symbolic y),B,context)))==0
        @test length(allresults(X((@symbolic x+y),B,context)))==0

        @test length(allresults(Xatom((@symbolic x),bindings,context)))==1
        @test length(allresults(Xatom((@symbolic y),bindings,context)))==1
        @test length(allresults(Xatom((@symbolic x+y),bindings,context)))==0       
    end

    @testset "Plain Arguments" begin
        p1=SCAM.Patterns.makepattern((@symbolic x+y))
        p2=SCAM.Patterns.makepattern((@symbolic x+y),:y)
        p3=SCAM.Patterns.makepattern((@symbolic x+y),:x,:y)
        p4=SCAM.Patterns.makepattern((@symbolic x+x),:x)
        bindings=Dict{Symbol,Any}()
        context=nothing

        @test length(allresults(p1((@symbolic x+x),bindings,context)))==0
        @test length(allresults(p1((@symbolic x+y),bindings,context)))==1
        @test length(allresults(p1((@symbolic y+x),bindings,context)))==0
        @test length(allresults(p1((@symbolic x*y),bindings,context)))==0
        @test length(allresults(p1((@symbolic x+y+z),bindings,context)))==0

        @test length(allresults(p2((@symbolic x+x),bindings,context)))==1
        @test length(allresults(p2((@symbolic x+y),bindings,context)))==1
        @test length(allresults(p2((@symbolic y+x),bindings,context)))==0
        @test length(allresults(p2((@symbolic x*y),bindings,context)))==0
        @test length(allresults(p2((@symbolic x+y+z),bindings,context)))==0

        @test length(allresults(p3((@symbolic x+x),bindings,context)))==1
        @test length(allresults(p3((@symbolic x+y),bindings,context)))==1
        @test length(allresults(p3((@symbolic y+x),bindings,context)))==1
        @test length(allresults(p3((@symbolic x*y),bindings,context)))==0
        @test length(allresults(p3((@symbolic x+y+z),bindings,context)))==0

        @test length(allresults(p4((@symbolic x+x),bindings,context)))==1
        @test length(allresults(p4((@symbolic x+y),bindings,context)))==0
        @test length(allresults(p4((@symbolic y+x),bindings,context)))==0
        @test length(allresults(p4((@symbolic x*y),bindings,context)))==0
        @test length(allresults(p4((@symbolic x+y+z),bindings,context)))==0

        p5=SCAM.Patterns.makepattern(@symbolic f(c(x),c(x),w,z))
        @test length(allresults(p5((@symbolic f(y,z,x,w)),bindings,context)))==0
    end

    @testset "Commuting Arguments" begin
        p1=SCAM.Patterns.makepattern(@symbolic f(commute(x),commute(y)))
        p2=SCAM.Patterns.makepattern(@symbolic f(commute(x),commute(y),w,z))
        p3=SCAM.Patterns.makepattern((@symbolic f(commute(X),commute(Y))),:X,:Y)
        bindings=Dict{Symbol,Any}()
        context=nothing
        
        @test length(allresults(p1((@symbolic f(x,y)),bindings,context)))==1
        @test length(allresults(p1((@symbolic f(y,x)),bindings,context)))==1

        @test length(allresults(p2((@symbolic f(y,w,x,z)),bindings,context)))==1
        @test length(allresults(p2((@symbolic f(y,z,x,w)),bindings,context)))==0

        @test length(allresults(p3((@symbolic f(x,y)),bindings,context)))==2
        @test length(allresults(p3((@symbolic f(x+y,y)),bindings,context)))==2
    end

    @testset "Slurping Arguments" begin
        p1=SCAM.Patterns.makepattern((@symbolic f(X...)),:X)
        p2=SCAM.Patterns.makepattern((@symbolic f(X...,X...)),:X)
        p3=SCAM.Patterns.makepattern((@symbolic f(X...,Y...)),:X,:Y)
        p4=SCAM.Patterns.makepattern((@symbolic f(commute(z),X...,X...)),:X)
        bindings=Dict{Symbol,Any}()
        context=nothing

        @test length(allresults(p1((@symbolic f(a,b,c)),bindings,context)))==1
        @test length(allresults(p1((@symbolic f(a,b)),bindings,context)))==1

        @test length(allresults(p2((@symbolic f(a,a,a)),bindings,context)))==0
        @test length(allresults(p2((@symbolic f(a,b,a,b)),bindings,context)))==1
        @test length(allresults(p2((@symbolic f(a,z,b,a,b)),bindings,context)))==0
        
        @test length(allresults(p3((@symbolic f(a,b,c)),bindings,context)))==4
        
        @test length(allresults(p4((@symbolic f(a,a,a)),bindings,context)))==0
        @test length(allresults(p4((@symbolic f(a,b,a,b)),bindings,context)))==0
        @test length(allresults(p4((@symbolic f(a,z,b,a,b)),bindings,context)))==1
    end

    @testset "Variable Head" begin
        p1=SCAM.Patterns.makepattern((@symbolic F(X...,c(Y),Z...)),:F,:X,:Y,:Z)
        bindings=Dict{Symbol,Any}()
        context=nothing
        b=firstresult(p1((@symbolic f(w,x,c(y),z)),bindings,context))
        @test b[:X]==[(@symbolic w),(@symbolic x)]
        @test b[:Y]==@symbolic y
        @test b[:Z]==[(@symbolic z)]
        @test b[:F]==@symbolic f
    end

    @testset "SingletonWrap" begin
        p1=SCAM.Patterns.makepattern((@symbolic f(singletonwrap(+,+(X,Y...)))),:X,:Y)
        bindings=Dict{Symbol,Any}()
        context=nothing
        b1=firstresult(p1((@symbolic f(x+y+z)),bindings,context))
        @test b1[:X]==@symbolic x
        @test b1[:Y]==[(@symbolic y),(@symbolic z)]
        @test length(allresults(p1((@symbolic f(x+y+z)),bindings,context)))==2
        b2=firstresult(p1((@symbolic f(g(y))),bindings,context))
        @test b2[:X]==@symbolic g(y)
        @test b2[:Y]==[]
        @test length(allresults(p1((@symbolic f(g(y))),bindings,context)))==1
    end
    
end


