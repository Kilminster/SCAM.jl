using Test
using SCAM

@testset "Derivatives" begin
    
    EXAMPLES=[((@symbolic ∂{x+x,x}),
               (@symbolic 2)),

              ((@symbolic ∂{x^2/x,x}),
               (@symbolic 1)),

              ((@symbolic ∂{x^2+b*x+c,x}),
               (@symbolic b+2*x)),

              ((@symbolic ∂{sin(exp(x)),x}),
               (@symbolic cos(exp(x))*exp(x))),

              ((@symbolic ∂{sin(x)^cos(x),x}),
               (@symbolic sin(x)^cos(x)*(-(sin(x)*log(sin(x)))+cos(x)^2/sin(x)))),
              ]

    for (input,expected) in EXAMPLES
        output=SCAM.simplify(input)

        if true
            println("Input:    $(input)")
            println("Output:   $(output)")
            println("Expected: $(expected)")
            println()
        end

        @test output==expected
    end

end
