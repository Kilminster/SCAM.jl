using Test
using SCAM

@testset "Canonical" begin

    @testset "Order" begin
        TESTTERMS=[(@symbolic (3*x*y)^2),
                   (@symbolic x^2*y^2),
                   (@symbolic cos(x+y)),
                   (@symbolic 9*cos(x+y))]
        
        @testset "Total" begin
            for a in TESTTERMS
                for b in TESTTERMS
                    @test (a==b)||(SCAM.CanonicalOrder.canonical_order(a,b)+
                                   SCAM.CanonicalOrder.canonical_order(b,a)==1)
                end
            end
        end

        @testset "Transitive" begin
            for a in TESTTERMS
                for b in TESTTERMS
                    for c in TESTTERMS
                        @test (!(SCAM.CanonicalOrder.canonical_order(a,b)&&
                                 SCAM.CanonicalOrder.canonical_order(b,c))||
                               SCAM.CanonicalOrder.canonical_order(a,c))

                        @test (!(SCAM.CanonicalOrder.canonical_order(b,a)&&
                                 SCAM.CanonicalOrder.canonical_order(c,b))||
                               SCAM.CanonicalOrder.canonical_order(c,a))
                    end
                end
            end
        end
        
    end
    
    @testset "Examples" begin

        EXAMPLES=[((@symbolic x+2+1),
                   (@symbolic 3+x)),

                  ((@symbolic x+2-(1+x)-1),
                   (@symbolic 0)),

                  ((@symbolic x+x+x+(y*x)+(y*x)),
                   (@symbolic 3*x+2*x*y)),

                  ((@symbolic 5*cos(x+y)+4*cos(y+x)+cos(y+x+0*z)),
                   (@symbolic 10*cos(x+y))),

                  ((@symbolic (x+y)^2*(x+y)*(y+x)+a),
                   (@symbolic a+(x+y)^4)),

                  ((@symbolic (x+y)^3*(x+y)^5*(x+y)),
                   (@symbolic (x+y)^9)),

                  ((@symbolic x/x),
                   (@symbolic x^(-1)*x)),

                  ((@symbolic x^2*y^2+(3*x*y)^2),
                   (@symbolic 10*x^2*y^2)),

                  ((@symbolic 1/(x-x)),
                   (@symbolic Undefined)),

                  ((@symbolic x-(5*x)),
                   (@symbolic (-4)*x)),

                  ((@symbolic -y+x),
                   (@symbolic x+(-1)*y)),

                  ((@symbolic 0*1/(x-x)),
                   (@symbolic Undefined))
                  
                  ]

        for (input,expected) in EXAMPLES
            output=SCAM.reduce(input,SCAM.Rules.canonical)

            if true
                println("Input:    $(input)")
                println("Output:   $(output)")
                println("Expected: $(expected)")
                println()
            end

            @test output==expected
        end

        
    end
end
