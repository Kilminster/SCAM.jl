using Test
using SCAM

@testset "Lambdas" begin

    EXAMPLES=[((@symbolic λ{x,x}(y)),
               (@symbolic y)),

              ((@symbolic λ{z,x}(y)),
               (@symbolic z)),

              ((@symbolic λ{f(x),f}(g)),
               (@symbolic g(x))),

              ((@symbolic λ{λ{f(f(x)),x},f}(g)(y)),
               (@symbolic g(g(y)))),

              ((@symbolic λ{λ{f(f(x)),x},f}(x)(y)),
               (@symbolic x(x(y)))),

              ((@symbolic λ{λ{f(f(x)),x},f}(h(x))(y)),
               (@symbolic h(x)(h(x)(y)))),

              ((@symbolic λ{(x + x) ^ 2, x}(0)),
               (@symbolic 0)),

              ((@symbolic λ{λ{f(x,y)+g(x,y),x,y},f,g}(a,b)(c,d)),
               (@symbolic a(c,d)+b(c,d))),

              ((@symbolic λ{f(x,y,z),x,y,z}(y,b,c)),
               (@symbolic f(y,b,c))),

              ((@symbolic λ{λ{f(x,y)+g(x,y),x,y},f,g}(x,y)(f,g)),
               (@symbolic x(f,g)+y(f,g))),

              ((@symbolic λ{x^2+y^2,x,y}(sin(x),cos(x))),
               (@symbolic cos(x)^2+sin(x)^2)),
              ]

    for (input,expected) in EXAMPLES
        output=SCAM.simplify(input)

        if true
            println("Input:    $(input)")
            println("Output:   $(output)")
            println("Expected: $(expected)")
            println()
        end

        @test output==expected
    end

end
