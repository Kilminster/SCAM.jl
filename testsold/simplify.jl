using Test
using SCAM

@testset "Simplify" begin

    EXAMPLES=[((@symbolic y+x-2*y),
               (@symbolic x-y))
              ]

    for (input,expected) in EXAMPLES
        output=SCAM.simplify(input)
        
        if true
                println("Input:    $(input)")
                println("Output:   $(output)")
                println("Expected: $(expected)")
                println()
            end
        
        @test output==expected
    end

    
end
